

CREATE TABLE `user` (
  `id` int(11)  auto_increment primary key,
  `photo` varchar(100),
  `lastname` varchar(100) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `activation_key` varchar(100),
  `forgot_key` varchar(100),
  `is_activated` int(1)  DEFAULT 0 COMMENT '0. NO 1. YES',
  `gender` int(1)  DEFAULT 1 COMMENT '1. Male 2. Female',
  `date_of_birth` DATE,
  `type` int(1)  DEFAULT 1 COMMENT '1. Customer 2. Admin',
  `status` int(1)  DEFAULT 1 COMMENT '0. INACTIVE 1. ACTIVE',
  `facebook_id` varchar(250),
  `google_id` varchar(250),
  `twitter_id` varchar(250),
  `created_at` datetime  
);

CREATE TABLE `category` (
  `id` int(11)  auto_increment primary key,
  `image` varchar(100)  NULL,
  `name` varchar(100) NOT NULL unique,
  `description` TEXT,
  `status` int(1)  DEFAULT '0' COMMENT '0. INACTIVE 1. ACTIVE',
  `created_at` datetime 
);

CREATE TABLE `tag` (
  `id` int(11)  auto_increment primary key,
  `name` varchar(100) NOT NULL unique,
  `status` int(1)  DEFAULT '0' COMMENT '0. INACTIVE 1. ACTIVE',
  `created_at` datetime 
);

CREATE TABLE `series` (
  `id` int(11) auto_increment primary key,
  `name` varchar(100) NOT NULL,
  `description` text,
  `image` varchar(100) ,
  `status` int(1)  DEFAULT '0' COMMENT '0. INACTIVE 1. ACTIVE',
  `category_id` int(11) NOT NULL,
  `created_at` datetime 
);

CREATE TABLE `vr` (
  `id` int(11) auto_increment primary key,
  `image` varchar(100) ,
  `file` text NOT NULL,
  `name` varchar(100) NOT NULL,
  `tags` text,
  `description` text,
  `location` text,
  `views` int(11),
  `downloaded` int(11),
  `category_id` int(11),
  `series_id` int(11),
  `status` int(1)  DEFAULT '0' COMMENT '0. INACTIVE 1. ACTIVE',
  `is_featured` int(1)  DEFAULT '0' COMMENT '0. No 1. Yes',
  `coming_soon` int(1)  DEFAULT '0' COMMENT '0. No 1. Yes',
  `created_at` datetime 
);


CREATE TABLE `vr_comment` (
  `id` int(11) auto_increment primary key,
  `comment` text,
  `user_id` int(11) not null,
  `vr_id` int(11) not null,
  `status` int(1)  DEFAULT '0' COMMENT '0. INACTIVE 1. ACTIVE',
  `created_at` datetime 
);

CREATE TABLE `vr_favourite` (
  `id` int(11) auto_increment primary key,
  `user_id` int(11) not null,
  `vr_id` int(11) not null,
  `created_at` datetime 
);
