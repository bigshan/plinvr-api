<?php include 'header.php' ?>
<!-- Page Content  -->
<script>
   var tags = '<?php echo count($tags) ?>';
</script>
<div id="content-page" class="content-page">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-12">
            <div class="iq-card">
               <div class="iq-card-header d-flex justify-content-between">
                  <div class="iq-header-title">
                     <h4 class="card-title"><?php echo $vr ? 'Update ' . $vr->name : 'Add VR' ?></h4>
                  </div>
               </div>
               <div class="iq-card-body">
                  <form action="<?php echo BASE_URL;  ?>vr/<?php echo $vr ? 'update' : 'add' ?>" enctype="multipart/form-data" method="post">
                     <div class="row">
                        <div class="col-lg-7">
                           <div class="row">
                              <div class="col-12 form-group">
                                 <input type="text" class="form-control" placeholder="Title" name="name" value="<?php echo  $vr ? $vr->name : '' ?>">
                              </div>
                              <div class="col-12 form_gallery form-group">
                                 <label id="gallery2" for="form_gallery-upload">Upload Image</label>
                                 <input type="hidden" name="id" value="<?php echo $vr ? $vr->id : ''; ?>">

                                 <input data-name="#gallery2" id="form_gallery-upload" name="image" class="form_gallery-upload" type="file" accept=".png, .jpg, .jpeg">
                              </div>
                              <div class="col-md-12 form-group">
                                 <select class="form-control" id="exampleFormControlSelect1" name="category_id">
                                    <option disabled="">Choose Category</option>
                                    <?php foreach ($categories as $category) {
                                    ?>
                                       <option value="<?php echo $category->id; ?>" <?php echo  $vr && $vr->category_id == $category->id   ? 'selected' : '' ?>><?php echo $category->name; ?></option>
                                    <?php
                                    } ?>
                                 </select>
                              </div>

                              <div class="col-12 form-group">
                                 <textarea id="text" name="description" rows="5" class="form-control" placeholder="Description"><?php echo  $vr ? $vr->description : '' ?></textarea>
                              </div>
                              <div class="col-12 form-group">
                                 <textarea id="text" name="location" rows="5" class="form-control" placeholder="Location"><?php echo  $vr ? $vr->location : '' ?></textarea>
                              </div>
                           </div>
                        </div>
                        <div class="col-lg-5">
                           <?php if ($vr) { ?>
                              <div class="d-block position-relative">
                                 <input type="file" name="file" class="d-block"><br />
                                 <iframe class=" d-block  w-100 h-lg-down-100 position-xl-relative top-0 bottom-0 border-0" height="250" src="<?php echo $vr->absolute_file_path ?>"></iframe>
                                 <div style="clear: both;"></div>
                              </div>
                           <?php } else { ?>
                              <div class="d-block position-relative">
                                 <div class="form_video-upload">
                                    <input type="file" name="file">
                                    <p>Upload VR</p>
                                 </div>
                              </div>
                           <?php } ?>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-sm-12 form-group">
                           <div class="form-group radio-box">
                              <label>Is Featured? </label>
                              <div class="radio-btn">
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadio13" name="is_featured" class="custom-control-input" value="0" <?php echo  $vr && $vr->is_featured == 0 ? 'checked' : '' ?>> <label class="custom-control-label" for="customRadio13">No</label>
                                 </div>
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadio12" name="is_featured" class="custom-control-input" value="1" <?php echo  $vr && $vr->is_featured == 1 ? 'checked' : '' ?>>
                                    <label class="custom-control-label" for="customRadio12">Yes </label>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-12 form-group">
                           <div class="form-group radio-box">
                              <label>Coming Soon? </label>
                              <div class="radio-btn">
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadio9" name="coming_soon" class="custom-control-input" value="0" <?php echo  $vr && $vr->coming_soon == 0 ? 'checked' : '' ?>> <label class="custom-control-label" for="customRadio9">No</label>
                                 </div>
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadio10" name="coming_soon" class="custom-control-input" value="1" <?php echo  $vr && $vr->coming_soon == 1 ? 'checked' : '' ?>>
                                    <label class="custom-control-label" for="customRadio10">Yes </label>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-12 form-group">
                           <div class="form-group radio-box">
                              <label>Status</label>
                              <div class="radio-btn">
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadio6" name="status" class="custom-control-input" value="1" <?php echo  $vr && $vr->status == 1 ? 'checked' : '' ?>> <label class="custom-control-label" for="customRadio6">Publish</label>
                                 </div>
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadio7" name="status" class="custom-control-input" value="0" <?php echo  $vr && $vr->status == 0 ? 'checked' : '' ?>>
                                    <label class="custom-control-label" for="customRadio7">Unpublish </label>
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="col-sm-12 form-group">
                           <div class="form-group">
                              <label>Tags</label>
                              <div class="">
                                 <?php
                                 $count = 1;
                                 foreach ($tags as $tag) {
                                 ?>
                                    <div class="custom-control custom-control-inline">
                                       <input type="checkbox" onclick="addtags()" id="chk<?php echo $count ?>" name="chk<?php echo $count ?>" class="custom-control-input" value="<?php echo $tag->name ?>" <?php echo  strpos($vr->tags, $tag->name) !== false ? 'checked' : '' ?>>
                                       <label class="custom-control-label" for="chk<?php echo $count ?>"><?php echo $tag->name ?></label>
                                    </div>
                                 <?php $count++;
                                 } ?>
                                 <input type="hidden" value="<?php echo $vr->tags ?>" name="tags" id="tags" />
                              </div>
                           </div>
                        </div>
                        <div class="col-12 form-group ">
                           <button type="submit" class="btn btn-primary"><?php echo $vr ? 'Update' : 'Add' ?></button>
                           <a type="reset" href="<?php echo BASE_URL ?>/admin/vrs" class="btn btn-danger">cancel</a>
                        </div>
                     </div>
                  </form>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include 'footer.php' ?>
<script>
   function addtags() {
      var tagsLength = parseInt(tags);
      var tagsString = "";
      for (var i = 1; i <= tagsLength; i++) {
         if ($("#chk" + i).prop("checked") == true) {
            if (tagsString != "") {
               tagsString += ", ";
            }
            tagsString += $("#chk" + i).val();
         }
      }

      $("#tags").val(tagsString);
   }
</script>