<?php include 'header.php' ?>
<!-- Page Content  -->
<div id="content-page" class="content-page">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-12">
            <div class="iq-card">
               <div class="iq-card-header d-flex justify-content-between">
                  <div class="iq-header-title">
                     <h4 class="card-title">VR Lists</h4>
                  </div>
                  <div class="iq-card-header-toolbar d-flex align-items-center">
                     <a href="<?php echo BASE_URL ?>admin/new-vr/" class="btn btn-primary">Add VR</a>
                  </div>
               </div>
               <div class="iq-card-body">
                  <div class="table-view">
                     <table class="data-tables table movie_table " style="width:100%">
                        <thead>
                           <tr>
                              <th>VR</th>
                              <th>Description</th>
                              <th>Category</th>
                              <th>Location</th>
                              <th>Tags</th>
                              <th>Featured</th>
                              <th>Coming Soon</th>
                              <th>Views</th>
                              <th></th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php foreach ($vrs as  $vr) {
                           ?>
                              <tr>
                                 <td>
                                    <div class="media align-items-center">
                                       <div class="iq-movie">
                                          <a target="_blank" href="<?php echo APP_URL ?>vr-preview/<?php echo $vr->id ?>"><img src="<?php echo $vr->thumbnail ?>" class="img-border-radius avatar-40 img-fluid" alt=""></a>
                                       </div>
                                       <div class="media-body text-white text-left ml-3">
                                          <p class="mb-0"><a target="_blank" href="<?php echo APP_URL ?>vr-preview/<?php echo $vr->id ?>"><?php echo $vr->name ?></a></p>
                                       </div>
                                    </div>
                                 </td>
                                 <td class="text-left"><?php echo $vr->description ?></td>
                                 <td><?php echo $vr->category->name ?></td>
                                 <td><?php echo $vr->location ?></td>
                                 <td><?php echo $vr->tags ?></td>
                                 <td><?php echo $vr->is_featured == 1 ? 'Yes' : 'No' ?></td>
                                 <td><?php echo $vr->coming_soon == 1 ? 'Yes' : 'No' ?></td>
                                 <td><?php echo $vr->views ?></td>
                                 <td>
                                    <div class="flex align-items-center list-user-action">
                                       <a class="iq-bg-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" href="<?php echo BASE_URL ?>admin/new-vr/<?php echo $vr->id; ?>"><i class="ri-pencil-line"></i></a>
                                       <a class="iq-bg-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="<?php echo BASE_URL ?>vr/remove/<?php echo $vr->id; ?>"><i class="ri-delete-bin-line"></i></a>
                                    </div>
                                 </td>
                              </tr>
                           <?php } ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include 'footer.php' ?>