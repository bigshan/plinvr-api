</div>
<!-- Wrapper END -->
<!-- Footer -->
<footer class="iq-footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-6">
                <ul class="list-inline mb-0">
                    <li class="list-inline-item"><a href="privacy-policy.html">Privacy Policy</a></li>
                    <li class="list-inline-item"><a href="terms-of-service.html">Terms of Use</a></li>
                </ul>
            </div>
            <div class="col-lg-6 text-right">
                Copyright 2020 <a href="#">Streamit</a> All Rights Reserved.
            </div>
        </div>
    </div>
</footer>
<!-- Footer END -->
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="<?php echo BASE_URL; ?>templates/assets/js/jquery.min.js"></script>
<script src="<?php echo BASE_URL; ?>templates/assets/js/popper.min.js"></script>
<script src="<?php echo BASE_URL; ?>templates/assets/js/bootstrap.min.js"></script>
<script src="<?php echo BASE_URL; ?>templates/assets/js/jquery.dataTables.min.js"></script>
<script src="<?php echo BASE_URL; ?>templates/assets/js/dataTables.bootstrap4.min.js"></script>
<!-- Appear JavaScript -->
<script src="<?php echo BASE_URL; ?>templates/assets/js/jquery.appear.js"></script>
<!-- Countdown JavaScript -->
<script src="<?php echo BASE_URL; ?>templates/assets/js/countdown.min.js"></script>
<!-- Counterup JavaScript -->
<script src="<?php echo BASE_URL; ?>templates/assets/js/waypoints.min.js"></script>
<script src="<?php echo BASE_URL; ?>templates/assets/js/jquery.counterup.min.js"></script>
<!-- Wow JavaScript -->
<script src="<?php echo BASE_URL; ?>templates/assets/js/wow.min.js"></script>
<!-- Slick JavaScript -->
<script src="<?php echo BASE_URL; ?>templates/assets/js/slick.min.js"></script>
<!-- Owl Carousel JavaScript -->
<script src="<?php echo BASE_URL; ?>templates/assets/js/owl.carousel.min.js"></script>
<!-- Magnific Popup JavaScript -->
<script src="<?php echo BASE_URL; ?>templates/assets/js/jquery.magnific-popup.min.js"></script>
<!-- Smooth Scrollbar JavaScript -->
<script src="<?php echo BASE_URL; ?>templates/assets/js/smooth-scrollbar.js"></script>
<!-- Chart Custom JavaScript -->
<script src="<?php echo BASE_URL; ?>templates/assets/js/chart-custom.js"></script>
<!-- Custom JavaScript -->
<script src="<?php echo BASE_URL; ?>templates/assets/js/custom.js"></script>
</body>


<!-- Mirrored from iqonic.design/themes/streamitnew/dashboard/html/theme/category-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 05 Sep 2020 07:05:34 GMT -->

</html>