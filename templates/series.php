<?php include 'header.php' ?>
<!-- Page Content  -->
<div id="content-page" class="content-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">

                        </div>
                    </div>
                    <div class="iq-card-body">
                        <div class="media align-items-center">
                            <div class="iq-movie">
                                <a href="javascript:void(0);"><img src="<?php echo BASE_URL . $series->image ?>" class="img-border-radius  img-fluid" alt="" style="width:200px"></a>
                            </div>
                            <div class="media-body text-white text-left ml-3">
                                <p class="mb-0"><?php echo $series->name ?></p>
                                <p class="mb-0"><?php echo $series->description ?></p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title"><?php echo $vr ? 'Update ' . $vr->name : 'Add VR' ?></h4>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <form action="<?php echo BASE_URL;  ?>vr/add" enctype="multipart/form-data" method="post">
                            <div class="row">
                                <div class="col-lg-7">
                                    <div class="row">
                                        <div class="col-12 form-group">
                                            <input type="text" class="form-control" placeholder="Title" name="name">
                                        </div>
                                        <div class="col-12 form_gallery form-group">
                                            <label id="gallery2" for="form_gallery-upload">Upload Image</label>
                                            <input type="hidden" name="series_id" value="<?php echo $series->id; ?>">

                                            <input data-name="#gallery2" id="form_gallery-upload" name="image" class="form_gallery-upload" type="file" accept=".png, .jpg, .jpeg">
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <select class="form-control" id="exampleFormControlSelect1" name="category_id">
                                                <option disabled="">Choose Category</option>
                                                <?php foreach ($categories as $category) {
                                                ?>
                                                    <option value="<?php echo $category->id; ?>"><?php echo $category->name; ?></option>
                                                <?php
                                                } ?>
                                            </select>
                                        </div>

                                        <div class="col-12 form-group">
                                            <textarea id="text" name="description" rows="5" class="form-control" placeholder="Description"></textarea>
                                        </div>
                                        <div class="col-12 form-group">
                                            <textarea id="text" name="location" rows="5" class="form-control" placeholder="Location"></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5">
                                    <div class="d-block position-relative">
                                        <div class="form_video-upload">
                                            <input type="file" name="file">
                                            <p>Upload VR</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 form-group">
                                    <div class="form-group radio-box">
                                        <label>Is Featured?</label>
                                        <div class="radio-btn">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio"  name="is_featured" id="customRadio1" class="custom-control-input" value="0" checked> 
                                                <label class="custom-control-label" for="customRadio1" >No</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio"  name="is_featured" id="customRadio2" class="custom-control-input" value="1">
                                                <label class="custom-control-label"  for="customRadio2">Yes </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <div class="form-group radio-box">
                                        <label>Coming Soon?</label>
                                        <div class="radio-btn">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio"  name="coming_soon" id="customRadio3"  class="custom-control-input" value="0"> 
                                                <label class="custom-control-label" for="customRadio3">No</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio"  name="coming_soon"  id="customRadio4" class="custom-control-input" value="1">
                                                <label class="custom-control-label" for="customRadio4">Yes </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <div class="form-group radio-box">
                                        <label>Status</label>
                                        <div class="radio-btn">
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio5" name="status" class="custom-control-input" value="1"> 
                                                <label class="custom-control-label"  for="customRadio5">Publish</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="customRadio6" name="status" class="custom-control-input" value="0">
                                                <label class="custom-control-label" for="customRadio6">Unpublish </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 form-group ">
                                    <button type="submit" class="btn btn-primary">Add</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">VR Lists</h4>
                        </div>
                    </div>
                    <div class="iq-card-body">
                        <div class="table-view">
                            <table class="data-tables table movie_table " style="width:100%">
                                <thead>
                                    <tr>
                                        <th>VR</th>
                                        <th>Description</th>
                                        <th>Category</th>
                                        <th>Views</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($vrs as  $vr) {
                                    ?>
                                        <tr>
                                            <td>
                                                <div class="media align-items-center">
                                                    <div class="iq-movie">
                                                        <a target="_blank" href="<?php echo APP_URL ?>vr-preview/<?php echo $vr->id ?>"><img src="<?php echo  $vr->thumbnail ?>" class="img-border-radius avatar-40 img-fluid" alt=""></a>
                                                    </div>
                                                    <div class="media-body text-white text-left ml-3">
                                                        <p class="mb-0"><a target="_blank" href="<?php echo APP_URL ?>vr-preview/<?php echo $vr->id ?>"><?php echo $vr->name ?></a></p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><?php echo $vr->description ?></td>
                                            <td><?php echo $vr->category->name ?></td>
                                            <td><?php echo $vr->views ?></td>
                                            <td>
                                                <div class="flex align-items-center list-user-action">
                                                    <a class="iq-bg-success" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" href="<?php echo BASE_URL ?>admin/new-vr/<?php echo $vr->id; ?>"><i class="ri-pencil-line"></i></a>
                                                    <a class="iq-bg-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="<?php echo BASE_URL ?>vr/remove/<?php echo $vr->id; ?>"><i class="ri-delete-bin-line"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php' ?>