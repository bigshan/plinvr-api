<!doctype html>
<html lang="en">


<!-- Mirrored from iqonic.design/themes/streamitnew/dashboard/html/theme/category-list.html by HTTrack Website Copier/3.x [XR&CO'2014], Sat, 05 Sep 2020 07:05:34 GMT -->

<head>
   <!-- Required meta tags -->
   <meta charset="utf-8">
   <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <title>PLINVR - Places In Virtual Reality</title>
   <!-- Favicon -->
   <link rel="shortcut icon" href="<?php echo BASE_URL; ?>templates/assets/images/logo.ico" />
   <!-- Bootstrap CSS -->
   <link rel="stylesheet" href="<?php echo BASE_URL; ?>templates/assets/css/bootstrap.min.css">
   <link rel="stylesheet" href="<?php echo BASE_URL; ?>templates/assets/css/dataTables.bootstrap4.min.css">
   <!-- Typography CSS -->
   <link rel="stylesheet" href="<?php echo BASE_URL; ?>templates/assets/css/typography.css">

   <!-- Style CSS -->
   <link rel="stylesheet" href="<?php echo BASE_URL; ?>templates/assets/css/style.css">
   <!-- Responsive CSS -->
   <link rel="stylesheet" href="<?php echo BASE_URL; ?>templates/assets/css/responsive.css">
</head>

<body>
   <!-- loader Start -->
   <div id="loading">
      <div id="loading-center">
      </div>
   </div>
   <!-- loader END -->
   <!-- Wrapper Start -->
   <div class="wrapper">
      <!-- Sidebar  -->
      <div class="iq-sidebar">
         <div class="iq-sidebar-logo d-flex justify-content-between">
            <a href="<?php echo APP_URL; ?>" class="header-logo">
               <img src="<?php echo BASE_URL; ?>templates/assets/images/logo.png" class="img-fluid rounded-normal" alt="">
               <div class="logo-title">
                  <span class="text-primary text-uppercase">PLINVR</span>
               </div>
            </a>
            <div class="iq-menu-bt-sidebar">
               <div class="iq-menu-bt align-self-center">
                  <div class="wrapper-menu">
                     <div class="main-circle"><i class="las la-bars"></i></div>
                  </div>
               </div>
            </div>
         </div>
         <div id="sidebar-scrollbar">
            <nav class="iq-sidebar-menu">
               <ul id="iq-sidebar-toggle" class="iq-menu">
                  <li>
                     <a href="<?php echo BASE_URL; ?>admin/comments" class="iq-waves-effect"><i class="las la-comments"></i><span>Comment</span></a>
                  </li>
                  <li class="<?php echo strpos($_SERVER['REQUEST_URI'], 'users')  !==  false ? 'active' : '' ?>">
                     <a href="<?php echo BASE_URL; ?>admin/users" class="iq-waves-effect"><i class="las la-user-friends"></i><span>User</span></a>
                  </li>
                  <li class="">
                     <a href="#tag" class="iq-waves-effect collapsed" data-toggle="collapse" aria-expanded="<?php echo strpos($_SERVER['REQUEST_URI'], 'tag')  !==  false ? 'true' : 'false' ?>"><i class="las la-list-ul"></i><span>Tag</span><i class="ri-arrow-right-s-line iq-arrow-right"></i></a>
                     <ul id="tag" class="iq-submenu collapse <?php echo strpos($_SERVER['REQUEST_URI'], 'tag')  !==  false ? 'show' : '' ?>" data-parent="#iq-sidebar-toggle">
                        <li class="<?php echo strpos($_SERVER['REQUEST_URI'], 'new-tag')  !==  false ? 'active' : '' ?>"><a href="<?php echo BASE_URL; ?>admin/new-tag"><i class="las la-user-plus"></i>Add Tag</a></li>
                        <li class="<?php echo strpos($_SERVER['REQUEST_URI'], 'tags')  !==  false ? 'active' : '' ?>"><a href="<?php echo BASE_URL; ?>admin/tags"><i class="las la-eye"></i>Tag List</a></li>
                     </ul>
                  </li>
                  <li class="">
                     <a href="#category" class="iq-waves-effect collapsed" data-toggle="collapse" aria-expanded="<?php echo strpos($_SERVER['REQUEST_URI'], 'categ')  !==  false ? 'true' : 'false' ?>"><i class="las la-list-ul"></i><span>Category</span><i class="ri-arrow-right-s-line iq-arrow-right"></i></a>
                     <ul id="category" class="iq-submenu collapse <?php echo strpos($_SERVER['REQUEST_URI'], 'categ')  !==  false ? 'show' : '' ?>" data-parent="#iq-sidebar-toggle">
                        <li class="<?php echo strpos($_SERVER['REQUEST_URI'], 'new-category')  !==  false ? 'active' : '' ?>"><a href="<?php echo BASE_URL; ?>admin/new-category"><i class="las la-user-plus"></i>Add Category</a></li>
                        <li class="<?php echo strpos($_SERVER['REQUEST_URI'], 'categories')  !==  false ? 'active' : '' ?>"><a href="<?php echo BASE_URL; ?>admin/categories"><i class="las la-eye"></i>Category List</a></li>
                     </ul>
                  </li>
                  <li>
                     <a href="#movie" class="iq-waves-effect collapsed" data-toggle="collapse" aria-expanded="<?php echo strpos($_SERVER['REQUEST_URI'], 'admin/vr')  !==  false ? 'true' : 'false' ?>"><i class="las la-film"></i><span>VR</span><i class="ri-arrow-right-s-line iq-arrow-right"></i></a>
                     <ul id="movie" class="iq-submenu collapse <?php echo strpos($_SERVER['REQUEST_URI'], 'admin/vr')  !==  false ? 'show' : '' ?>" data-parent="#iq-sidebar-toggle">
                        <li class="<?php echo strpos($_SERVER['REQUEST_URI'], 'new-vr')  !==  false ? 'active' : '' ?>"><a href="<?php echo BASE_URL; ?>admin/new-vr"><i class="las la-user-plus"></i>Add VR</a></li>
                        <li class="<?php echo strpos($_SERVER['REQUEST_URI'], 'vrs')  !==  false ? 'active' : '' ?>"><a href="<?php echo BASE_URL; ?>admin/vrs"><i class="las la-eye"></i>VR List</a></li>
                     </ul>
                  </li>
                  <li>
                     <a href="#show" class="iq-waves-effect collapsed" data-toggle="collapse" aria-expanded="<?php echo strpos($_SERVER['REQUEST_URI'], 'series')  !==  false ? 'true' : 'false' ?>"><i class="las la-film"></i><span>Series</span><i class="ri-arrow-right-s-line iq-arrow-right"></i>
                     </a>
                     <ul id="show" class="iq-submenu collapse <?php echo strpos($_SERVER['REQUEST_URI'], 'series')  !==  false ? 'show' : '' ?>" data-parent="#iq-sidebar-toggle">
                        <li class="<?php echo strpos($_SERVER['REQUEST_URI'], 'new-series')  !==  false ? 'active' : '' ?>"><a href="<?php echo BASE_URL; ?>admin/new-series"><i class="las la-user-plus"></i>Add Series</a></li>
                        <li class="<?php echo strpos($_SERVER['REQUEST_URI'], 'all-series')  !==  false ? 'active' : '' ?>"><a href="<?php echo BASE_URL; ?>admin/all-series"><i class="las la-eye"></i>All Series</a></li>
                     </ul>
                  </li>

               </ul>
               </li>

               </ul>
            </nav>
         </div>
      </div>
      <!-- TOP Nav Bar -->
      <div class="iq-top-navbar">
         <div class="iq-navbar-custom">
            <nav class="navbar navbar-expand-lg navbar-light p-0">
               <div class="iq-menu-bt d-flex align-items-center">
                  <div class="wrapper-menu">
                     <div class="main-circle"><i class="las la-bars"></i></div>
                  </div>
                  <div class="iq-navbar-logo d-flex justify-content-between">
                     <a href="index-2.html" class="header-logo">
                        <img src="<?php echo BASE_URL; ?>templates/assets/images/logo.png" class="img-fluid rounded-normal" alt="">
                        <div class="logo-title">
                           <span class="text-primary text-uppercase">PLINVR</span>
                        </div>
                     </a>
                  </div>
               </div>
               
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-label="Toggle navigation">
                  <i class="ri-menu-3-line"></i>
               </button>
               <div class="collapse navbar-collapse" id="navbarSupportedContent">
                  
               </div>
            </nav>
         </div>
      </div>
      <!-- TOP Nav Bar END -->