<?php include 'header.php' ?>
<!-- Page Content  -->
<div id="content-page" class="content-page">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-12">
            <div class="iq-card">
               <div class="iq-card-header d-flex justify-content-between">
                  <div class="iq-header-title">
                     <h4 class="card-title"><?php echo $tag ? 'Update ' . $tag->name : 'Add Tag' ?></h4>
                  </div>
               </div>
               <div class="iq-card-body">
                  <div class="row">
                     <div class="col-lg-12">
                        <form action="<?php echo BASE_URL;  ?>tag/<?php echo $tag ? 'update' : 'add' ?>" enctype="multipart/form-data" method="post">
                           <div><?php echo $message; ?></div>
                           <div class="form-group">
                              <input type="hidden" name="id" value="<?php echo $tag ? $tag->id : ''; ?>">
                              <input type="text" class="form-control" placeholder="Name" name="name" value="<?php echo $tag ? $tag->name : ''; ?>">
                           </div>
                           
                           <div class="form-group radio-box">
                              <label>Status</label>
                              <div class="radio-btn">
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadio6" name="status" class="custom-control-input" value="1" <?php echo  $tag && $tag->status == 1 ? 'checked' : '' ?>>
                                    <label class="custom-control-label" for="customRadio6">Publish</label>
                                 </div>
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadio7" name="status" class="custom-control-input" value="0" <?php echo  $tag && $tag->status == 0 ? 'checked' : '' ?>>
                                    <label class="custom-control-label" for="customRadio7">Unpublish </label>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <button type="submit" class="btn btn-primary"><?php echo $tag ? 'Update' : 'Add' ?></button>
                              <a type="reset" href="<?php echo BASE_URL ?>/admin/categories" class="btn btn-danger">cancel</a>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include 'footer.php' ?>