<?php include 'header.php' ?>
<!-- Page Content  -->
<div id="content-page" class="content-page">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-12">
            <div class="iq-card">
               <div class="iq-card-header d-flex justify-content-between">
                  <div class="iq-header-title">
                     <h4 class="card-title"><?php echo $category ? 'Update ' . $category->name : 'Add Category' ?></h4>
                  </div>
               </div>
               <div class="iq-card-body">
                  <div class="row">
                     <div class="col-lg-12">
                        <form action="<?php echo BASE_URL;  ?>category/<?php echo $category ? 'update' : 'add' ?>" enctype="multipart/form-data" method="post">
                           <div><?php echo $message; ?></div>
                           <div class="form-group form_gallery">
                              <label id="gallery2" for="form_gallery-upload">Upload Image</label>
                              <input data-name="#gallery2" id="form_gallery-upload" class="form_gallery-upload" type="file" accept=".png, .jpg, .jpeg" name="image">
                           </div>
                           <div class="form-group">
                              <input type="hidden" name="id" value="<?php echo $category ? $category->id : ''; ?>">
                              <input type="text" class="form-control" placeholder="Name" name="name" value="<?php echo $category ? $category->name : ''; ?>">
                           </div>
                           <div class="form-group">
                              <textarea id="text" name="description" rows="5" class="form-control" placeholder="Description"><?php echo $category ? $category->description : ''; ?></textarea>
                           </div>
                           <div class="form-group radio-box">
                              <label>Status</label>
                              <div class="radio-btn">
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadio6" name="status" class="custom-control-input" value="1" <?php echo  $category && $category->status == 1 ? 'checked' : '' ?>>
                                    <label class="custom-control-label" for="customRadio6">Publish</label>
                                 </div>
                                 <div class="custom-control custom-radio custom-control-inline">
                                    <input type="radio" id="customRadio7" name="status" class="custom-control-input" value="0" <?php echo  $category && $category->status == 0 ? 'checked' : '' ?>>
                                    <label class="custom-control-label" for="customRadio7">Unpublish </label>
                                 </div>
                              </div>
                           </div>
                           <div class="form-group ">
                              <button type="submit" class="btn btn-primary"><?php echo $category ? 'Update' : 'Add' ?></button>
                              <a type="reset" href="<?php echo BASE_URL ?>admin/categories" class="btn btn-danger">cancel</a>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include 'footer.php' ?>