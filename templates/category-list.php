<?php include 'header.php' ?>
<!-- Page Content  -->
<div id="content-page" class="content-page">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-12">
            <div class="iq-card">
               <div class="iq-card-header d-flex justify-content-between">
                  <div class="iq-header-title">
                     <h4 class="card-title">Category Lists</h4>
                  </div>
                  <div class="iq-card-header-toolbar d-flex align-items-center">
                     <a href="<?php echo BASE_URL ?>admin/new-category" class="btn btn-primary">Add Category</a>
                  </div>
               </div>
               <div class="iq-card-body">
                  <div class="table-view">
                     <table class="data-tables table movie_table " style="width:100%">
                        <thead>
                           <tr>
                              <th style="width:10%;">No</th>
                              <th style="width:20%;">Name</th>
                              <th style="width:40%;">Description</th>
                              <th style="width:10%;">status</th>
                              <th style="width:20%;">Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php
                           $count = 1;
                           foreach ($categories as $category) {
                           ?>
                              <tr>
                                 <td><?php echo $count++; ?></td>
                                 <td>
                                    <div class="media align-items-center">
                                       <div class="iq-movie">
                                       <a target="_blank" href="<?php echo APP_URL ?>category/<?php echo $category->id ?>"><img src="<?php echo $category->small ?>" class="img-border-radius avatar-40 img-fluid" alt=""></a>
                                       </div>
                                       <div class="media-body text-white text-left ml-3">
                                          <p class="mb-0"><a target="_blank" href="<?php echo APP_URL ?>category/<?php echo $category->id ?>"><?php echo $category->name ?></a></p>
                                       </div>
                                    </div>
                                 </td>
                                 <td><?php echo $category->description ?></td>
                                 <td><?php echo $category->status == 1 ? 'Enabled' : 'Disabled'; ?></td>
                                 <td>
                                    <div class="flex align-items-center list-user-action">
                                       <a class="iq-bg-success" href="<?php echo BASE_URL ?>admin/new-category/<?php echo $category->id ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Edit" href="#"><i class="ri-pencil-line"></i></a>
                                       <a class="iq-bg-primary" href="<?php echo BASE_URL ?>category/remove/<?php echo $category->id ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="#"><i class="ri-delete-bin-line"></i></a>
                                    </div>
                                 </td>
                              </tr>
                           <?php
                           } ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include 'footer.php' ?>