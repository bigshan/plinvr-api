<?php include 'header.php' ?>
<!-- Page Content  -->
<div id="content-page" class="content-page">
   <div class="container-fluid">
      <div class="row">
         <div class="col-sm-12">
            <div class="iq-card">
               <div class="iq-card-header d-flex justify-content-between">
                  <div class="iq-header-title">
                     <h4 class="card-title">Comment Lists</h4>
                  </div>
                 
               </div>
               <div class="iq-card-body">
                  <div class="table-view">
                     <table class="data-tables table movie_table " style="width:100%">
                        <thead>
                           <tr>
                              <th style="width:10%;">No</th>
                              <th style="width:20%;">Comment</th>
                              <th style="width:40%;">VR</th>
                              <th style="width:10%;">User</th>
                              <th style="width:20%;">Action</th>
                           </tr>
                        </thead>
                        <tbody>
                           <?php
                           $count = 1;
                           foreach ($comments as $comment) {
                           ?>
                              <tr>
                                 <td><?php echo $count++; ?></td>
                                 <td>
                                    <div class="media align-items-center">
                                       
                                       <div class="media-body text-white text-left ml-3">
                                          <p class="mb-0"><?php echo $comment->comment ?></p>
                                       </div>
                                    </div>
                                 </td>
                                 <td><?php echo $comment->vr->name ?></td>
                                 <td><?php echo $comment->user->lastname; ?> <?php echo $comment->user->firstname; ?></td>
                                 <td>
                                    <div class="flex align-items-center list-user-action">
                                       <a class="iq-bg-primary" href="<?php echo BASE_URL ?>vr-comment/remove/<?php echo $comment->id ?>" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete" href="#"><i class="ri-delete-bin-line"></i></a>
                                    </div>
                                 </td>
                              </tr>
                           <?php
                           } ?>
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<?php include 'footer.php' ?>