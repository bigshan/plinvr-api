<?php include 'header.php' ?>
<!-- Page Content  -->
<div id="content-page" class="content-page">
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="iq-card">
                    <div class="iq-card-header d-flex justify-content-between">
                        <div class="iq-header-title">
                            <h4 class="card-title">Users Lists</h4>
                        </div>

                    </div>
                    <div class="iq-card-body">
                        <div class="table-view">
                            <table class="data-tables table movie_table " style="width:100%">
                                <thead>
                                    <tr>
                                        <th style="width:10%;">No</th>
                                        <th style="width:20%;">Name</th>
                                        <th style="width:40%;">Email</th>
                                        <th style="width:10%;">Gender</th>
                                        <th style="width:10%;">DOB</th>
                                        <th style="width:20%;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $count = 1;
                                    foreach ($users as $user) {
                                    ?>
                                        <tr>
                                            <td><?php echo $count++; ?></td>
                                            <td>
                                                <div class="media align-items-center">
                                                    <div class="media-body text-white text-left ml-3">
                                                        <p class="mb-0"><?php echo $user->lastname ?> <?php echo $user->firstname ?></p>
                                                    </div>
                                                </div>
                                            </td>
                                            <td><?php echo $user->email ?></td>
                                            <td><?php echo $user->gender == 1 ? 'Male' : 'Female'; ?></td>
                                            <td><?php echo $user->date_of_birth; ?></td>
                                            <td>
                                                <?php if ($user->status == 1) { ?>
                                                    <a class="btn btn-danger" href="<?php echo BASE_URL; ?>user/change-status/<?php echo $user->id; ?>/0"> Set Inactive</a>
                                                <?php } else { ?>
                                                    <a class="btn btn-danger" href="<?php echo BASE_URL; ?>user/change-status/<?php echo $user->id; ?>/1"> set Active</a>

                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php
                                    } ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include 'footer.php' ?>