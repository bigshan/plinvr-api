<?php
header("Access-Control-Allow-Origin: *");
//header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

ini_set("max_execution_time", "-1");
ini_set("memory_limit", "-1");
set_time_limit(0);
session_start();
/**
 * @package Plinvr
 */


require_once __DIR__ . '/configuration.php';
require_once __DIR__ . '/vendor/autoload.php';

//handle errors
if (MODE == 'development') {
    error_reporting(E_ALL);
    ini_set('display_errors', true);
} else {
    ini_set('display_errors', false);
    ini_set('log_errors', true);
    ini_set('error_log', 'errors.log');
    ini_set('log_errors_max_len', 1024);
}

//create upload structure
if (!file_exists('uploads/raw')) {
    mkdir('uploads/raw', 0777, true);
}

if (!file_exists('uploads/vr')) {
    mkdir('uploads/vr', 0777, true);
}

if (!file_exists('uploads/vr/extract')) {
    mkdir('uploads/vr/extract', 0777, true);
}

if (!file_exists('uploads/thumbnail')) {
    mkdir('uploads/thumbnail', 0777, true);
}


//process route
use \Plinvr\Core\Router;

$request = str_replace(BASE_URL, '', ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"));
$requestParam = explode("/", $request);
echo Router::route($requestParam);
