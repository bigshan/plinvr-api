<?php

namespace Plinvr\Manager;

use Plinvr\Adapter\DatabaseAdapter;

/**
 * @package Plinvr
 */

class VRManager extends DatabaseAdapter
{
    public function add($vr)
    {
        $data = [
            'file' => $vr->file,
            'name' => $vr->name,
            'description' => $vr->description,
            'location' => $vr->location,
            'is_featured' => $vr->is_featured,
            'coming_soon' => $vr->coming_soon,
            'image' => $vr->image,
            'category_id' => $vr->category_id,
            'status' => $vr->status,
            'views' => $vr->views,
            'tags' => $vr->tags,
            'downloaded' => $vr->downloaded,
            'series_id' => $vr->series_id,
            'created_at' => $this->db->now()
        ];

        $id = $this->db->insert('`vr`', $data);

        if ($id) {
            $vr->id = $id;
        }

        return $id ? $vr : null;
    }

    public function update($vr)
    {
        $data = [
            'file' => $vr->file,
            'name' => $vr->name,
            'description' => $vr->description,
            'image' => $vr->image,
            'category_id' => $vr->category_id,
            'location' => $vr->location,
            'is_featured' => $vr->is_featured,
            'coming_soon' => $vr->coming_soon,
            'status' => $vr->status,
            'views' => $vr->views,
            'tags' => $vr->tags,            
            'downloaded' => $vr->downloaded,
            'series_id' => $vr->series_id
        ];

        $this->db->where('id', $vr->id);
        $result =  $this->db->update('`vr`', $data) ? $vr : null;
        return $result;
    }

    public function delete($id)
    {
        $this->db->where('id', $id);

        return !$this->db->delete('`vr`');
    }
}
