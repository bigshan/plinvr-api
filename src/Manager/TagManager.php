<?php
namespace Plinvr\Manager;

use Plinvr\Adapter\DatabaseAdapter;

/**
 * @package Plinvr
 */

class TagManager extends DatabaseAdapter
{
    public function add($tag)
    {
        $data = [
            'name' => $tag->name,
            'status' => $tag->status,
            'created_at' => $this->db->now(),
        ];

        $id = $this->db->insert('`tag`', $data);

        if ($id) {
            $tag->id = $id;
        }

        return $id ? $tag : null;
    }

    public function update($tag)
    {
        $data = [
            'name' => $tag->name,
            'status' => $tag->status                     
        ];

        $this->db->where('id', $tag->id);

        return $this->db->update('`tag`', $data) ? $tag : null;
    }

    public function delete($id)
    {
        $this->db->where('id', $id);

        return !$this->db->delete('`tag`');
    }
}
