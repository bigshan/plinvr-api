<?php
namespace Plinvr\Manager;

use Plinvr\Adapter\DatabaseAdapter;

/**
 * @package Plinvr
 */

class SeriesManager extends DatabaseAdapter
{
    public function add($series)
    {
        $data = [
            'name' => $series->name,
            'description' => $series->description,
            'image' => $series->image,
            'category_id' => $series->category_id,
            'status' => $series->status,
            'created_at' => $this->db->now(),
        ];

        $id = $this->db->insert('`series`', $data);

        if ($id) {
            $series->id = $id;
        }

        return $id ? $series : null;
    }

    public function update($series)
    {
        $data = [
            'name' => $series->name,
            'description' => $series->description,
            'image' => $series->image,
            'category_id' => $series->category_id,
            'status' => $series->status                  
        ];

        $this->db->where('id', $series->id);

        return $this->db->update('`series`', $data) ? $series : null;
    }

    public function delete($id)
    {
        $this->db->where('id', $id);

        return !$this->db->delete('`series`');
    }
}
