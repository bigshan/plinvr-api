<?php

namespace Plinvr\Manager;

use Plinvr\Adapter\DatabaseAdapter;

/**
 * @package Plinvr
 */

class VRFavouriteManager extends DatabaseAdapter
{
    public function add($vr_favourite)
    {
        $data = [
            'user_id' => $vr_favourite->user_id,
            'vr_id' => $vr_favourite->vr_id,
            'created_at' => $this->db->now(),
        ];

        $id = $this->db->insert('`vr_favourite`', $data);

        if ($id) {
            $vr_favourite->id = $id;
        }
        return $id ? $vr_favourite : null;
    }



    public function delete($id)
    {
        $this->db->where('id', $id);

        return !$this->db->delete('`vr_favourite`');
    }
}
