<?php

namespace Plinvr\Manager;

use Plinvr\Adapter\DatabaseAdapter;

/**
 * @package Plinvr
 */

class UserManager extends DatabaseAdapter
{
    public function add($user)
    {
        $data = [
            'photo' => $user->photo,
            'lastname' => $user->lastname,
            'firstname' => $user->firstname,
            'email' => $user->email,
            'password' => $user->password,
            'activation_key' => $user->activation_key,
            'forgot_key' => $user->forgot_key,
            'is_activated' => $user->is_activated,
            'gender' => $user->gender,
            'date_of_birth' => $user->date_of_birth,
            'type' => $user->type,
            'status' => $user->status,
            'facebook_id' => $user->facebook_id,
            'google_id' => $user->google_id,
            'twitter_id' => $user->twitter_id,
            'created_at' => $this->db->now(),
        ];

        $id = $this->db->insert('user', $data);

        if ($id) {
            $user->id = $id;
        }

        return $id ? $user : null;
    }

    public function update($user)
    {
        $data = [
            'photo' => $user->photo,
            'lastname' => $user->lastname,
            'firstname' => $user->firstname,
            'email' => $user->email,
            'password' => $user->password,
            'activation_key' => $user->activation_key,
            'is_activated' => $user->is_activated,
            'forgot_key' => $user->forgot_key,
            'gender' => $user->gender,
            'date_of_birth' => $user->date_of_birth,
            'type' => $user->type,
            'status' => $user->status,
            'facebook_id' => $user->facebook_id,
            'google_id' => $user->google_id,
            'twitter_id' => $user->twitter_id
        ];

        $this->db->where('id', $user->id);

        return $this->db->update('user', $data) ? $user : null;
    }

    public function delete($id)
    {
        $this->db->where('id', $id);

        return !$this->db->delete('user');
    }
}
