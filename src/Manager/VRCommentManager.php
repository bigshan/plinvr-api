<?php
namespace Plinvr\Manager;

use Plinvr\Adapter\DatabaseAdapter;

/**
 * @package Plinvr
 */

class VRCommentManager extends DatabaseAdapter
{
    public function add($vr_comment)
    {
        $data = [
            'comment' => $vr_comment->comment,
            'user_id' => $vr_comment->user_id,
            'vr_id' => $vr_comment->vr_id,
            'status' => $vr_comment->status,
            'created_at' => $this->db->now(),
        ];

        $id = $this->db->insert('`vr_comment`', $data);

        if ($id) {
            $vr_comment->id = $id;
        }

        return $id ? $vr_comment : null;
    }

    public function update($vr_comment)
    {
        $data = [
            'comment' => $vr_comment->comment,
            'user_id' => $vr_comment->user_id,
            'vr_id' => $vr_comment->vr_id,
            'status' => $vr_comment->status                     
        ];

        $this->db->where('id', $vr_comment->id);

        return $this->db->update('`vr_comment`', $data) ? $vr_comment : null;
    }

    public function delete($id)
    {
        $this->db->where('id', $id);

        return !$this->db->delete('`vr_comment`');
    }
}
