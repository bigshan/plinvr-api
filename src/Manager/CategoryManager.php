<?php
namespace Plinvr\Manager;

use Plinvr\Adapter\DatabaseAdapter;

/**
 * @package Plinvr
 */

class CategoryManager extends DatabaseAdapter
{
    public function add($category)
    {
        $data = [
            'name' => $category->name,
            'description' => $category->description,
            'image' => $category->image,
            'status' => $category->status,
            'created_at' => $this->db->now(),
        ];

        $id = $this->db->insert('`category`', $data);

        if ($id) {
            $category->id = $id;
        }

        return $id ? $category : null;
    }

    public function update($category)
    {
        $data = [
            'name' => $category->name,
            'description' => $category->description,
            'image' => $category->image,
            'status' => $category->status                     
        ];

        $this->db->where('id', $category->id);

        return $this->db->update('`category`', $data) ? $category : null;
    }

    public function delete($id)
    {
        $this->db->where('id', $id);

        return !$this->db->delete('`category`');
    }
}
