<?php
namespace Plinvr\Messager;

/**
 * @package Plinvr
 */
use PHPMailer\PHPMailer\Exception;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;

class EmailMessager
{
    public function send($subject, $body, $senderEmail, $recipentEmail, $recipentName = null, $senderName = "Plinvr")
    {
        // Instantiation and passing `true` enables exceptions
        $mail = new PHPMailer(true);
        try {
            //Server settings
            if (ENABLE_MAIL_DEBUG) {
                $mail->SMTPDebug = SMTP::DEBUG_SERVER; // Enable verbose debug output
            }
            $mail->isSMTP(); // Send using SMTP
            $mail->Host = MAIL_HOST; // Set the SMTP server to send through
            $mail->SMTPAuth = true; // Enable SMTP authentication
            $mail->Username = MAIL_USERNAME; // SMTP username
            $mail->Password = MAIL_PASSWORD; // SMTP password
            $mail->SMTPSecure = 'tls'; 
            //$mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS; // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` also accepted
            $mail->Port = MAIL_PORT; // TCP port to connect to

            //Recipients
            $mail->setFrom($senderEmail, $senderName);
            $mail->addAddress($recipentEmail, $recipentName); // Add a recipient

            // Content
            $mail->isHTML(true); // Set email format to HTML
            $mail->Subject = $subject;
            $mail->Body = $body;

            $mail->send();
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
}
