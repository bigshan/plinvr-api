<?php

namespace Plinvr\Model;

/**
 * @package Plinvr
 */

class Tag
{
    /** properties */

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var int */
    public $status;
    
    const ACTIVE = 1;
    const INACTIVE = 0;

    public function __construct($name,  $status = 1, $id = null)
    {
        $this->name = $name;
        $this->id = $id;
        $this->status =  $status;
    }
}
