<?php

namespace Plinvr\Model;

/**
 * @package Plinvr
 */

class User
{
    /** properties */

    /** @var int */
    public $id;

    /** @var string */
    public $photo;

    /** @var string */
    public $lastname;

    /** @var string */
    public $firstname;

    /** @var string */
    public $email;

    /** @var int */
    public $password;

    /** @var string */
    public $activation_key;

    /** @var string */
    public $forgot_key;

    /** @var string */
    public $is_activated;

    /** @var string */
    public $gender;

    /** @var string */
    public $date_of_birth;

    /** @var string */
    public $type;

    /** @var string */
    public $status;

    /** @var string */
    public $facebook_id;

    /** @var string */
    public $google_id;

    /** @var string */
    public $twitter_id;

    /** @var string */
    public $created_at;

    /** @var string */
    public $full_path;

    const CUSTOMER = 1;
    const ADMIN = 2;
    const MALE = 1;
    const FEMALE = 2;
    const INACTIVE = 0;
    const ACTIVE = 1;
    const NO = 0;
    const YES = 1;

    public function __construct(
        $photo,
        $lastname,
        $firstname,
        $email,
        $password,
        $activation_key,
        $forgot_key = null,
        $is_activated = 0,
        $gender = null,
        $date_of_birth = null,
        $type = 1,
        $status = 1,
        $facebook_id = null,
        $google_id = null,
        $twitter_id = null,
        $created_at = null,
        $id = null
    ) {
        $this->photo = $photo;
        $this->full_path = BASE_URL . $photo;
        $this->lastname = $lastname;
        $this->id = $id;
        $this->firstname = $firstname;
        $this->email = $email;
        $this->password = $password;
        $this->activation_key = $activation_key;
        $this->forgot_key = $forgot_key;
        $this->is_activated = $is_activated;
        $this->gender = $gender;
        $this->date_of_birth = $date_of_birth;
        $this->type = $type;
        $this->status = $status;
        $this->facebook_id = $facebook_id;
        $this->google_id = $google_id;
        $this->twitter_id = $twitter_id;
        $this->created_at = $created_at;
    }
}
