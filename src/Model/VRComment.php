<?php

namespace Plinvr\Model;

/**
 * @package Plinvr
 */

use Plinvr\Repository\UserRepository;
use Plinvr\Repository\VRRepository;

class VRComment
{
    /** properties */

    /** @var int */
    public $id;

    /** @var string */
    public $comment;

    /** @var string */
    public $user_id;

    /** @var int */
    public $status;

    /** @var string */
    public $vr_id;

    /** @var string */
    public $created_at;

    /** @var object */
    public $vr;

    /** @var object */
    public $user;

    const ACTIVE = 1;
    const INACTIVE = 0;

    public function __construct($comment, $user_id, $vr_id,  $status = 1, $id = null, $created_at = null)
    {
        $this->comment = $comment;
        $this->user_id = $user_id;
        $this->vr_id = $vr_id;
        $this->id = $id;
        $this->status =  $status;
        $this->created_at =  $created_at;

        $userRepository = new UserRepository();
        $this->user =  $userRepository->getById($user_id);
        $vrRepository = new VRRepository();
        $this->vr =  $vrRepository->getOne($vr_id);
    }
}
