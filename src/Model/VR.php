<?php

namespace Plinvr\Model;

use Plinvr\Repository\CategoryRepository;

/**
 * @package Plinvr
 */

class VR
{
    /** properties */

    /** @var int */
    public $id;

    /** @var string */
    public $file;

    /** @var string */
    public $name;

    /** @var string */
    public $description;

    /** @var string */
    public $location;

    /** @var int */
    public $is_featured;

    /** @var int */
    public $status;

    /** @var int */
    public $views;

    /** @var int */
    public $series_id;

    /** @var int */
    public $downloaded;

    /** @var int */
    public $category_id;

    /** @var string */
    public $image;

    /** @var string */
    public $raw;

    /** @var string */
    public $small;

    /** @var array */
    public $category;

    /** @var string */
    public $absolute_file_path;

    /** @var string */
    public $created_at;

    /** @var string */
    public $tags;

    /** @var string */
    public $coming_soon;


    const ACTIVE = 1;
    const INACTIVE = 0;

    public function __construct($file, $name, $description, $image, $category_id, $location, $is_featured = 0, $coming_soon = 0, $series_id = null, $status = 1, $views = 0, $downloaded = 0, $tags = null, $id = null, $created_at = null)
    {
        $this->file = $file;
        $this->absolute_file_path = BASE_URL . "uploads/vr/" . $file;
        $this->name = $name;
        $this->description = $description;
        $this->category_id = $category_id;
        $this->location = $location;
        $this->is_featured = $is_featured;
        $this->coming_soon = $coming_soon;
        $this->image = $image;
        $this->small = BASE_URL . "uploads/small/" . $image;
        $this->thumbnail = BASE_URL . "uploads/thumbnail/" . $image;
        $this->raw = BASE_URL . "uploads/raw/" . $image;
        $this->id = $id;
        $this->status =  $status;
        $this->series_id =  $series_id;
        $this->downloaded =  $downloaded;
        $this->views =  $views;
        $this->tags =  $tags;
        $this->created_at =  $created_at;
        $categoryRepository = new CategoryRepository();
        $this->category = $categoryRepository->getOne($category_id);
    }
}
