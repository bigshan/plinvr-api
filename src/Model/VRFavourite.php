<?php

namespace Plinvr\Model;

/**
 * @package Plinvr
 */

use Plinvr\Repository\UserRepository;
use Plinvr\Repository\VRRepository;

class VRFavourite
{
    /** properties */

    /** @var int */
    public $id;

    /** @var string */
    public $user_id;

    /** @var string */
    public $vr_id;

     /** @var object */
     public $vr;

     /** @var object */
     public $user;

    public function __construct( $user_id, $vr_id, $id = null)
    {
        $this->user_id = $user_id;
        $this->vr_id = $vr_id;
        $this->id = $id;
        
        $userRepository = new UserRepository();
        $this->user =  $userRepository->getById($user_id);
        $vrRepository = new VRRepository();
        $this->vr =  $vrRepository->getOne($vr_id);
    }
}
