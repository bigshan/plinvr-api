<?php

namespace Plinvr\Model;

use Plinvr\Repository\CategoryRepository;
use Plinvr\Repository\VRRepository;

/**
 * @package Plinvr
 */

class Series
{
    /** properties */

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $description;

    /** @var int */
    public $status;

    /** @var int */
    public $category_id;

    /** @var string */
    public $image;

    /** @var string */
    public $raw;

    /** @var string */
    public $thumbnail;

    /** @var string */
    public $small;

    /** @var arrayy */
    public $category;

    /** @var arrayy */
    public $episodes;

    const ACTIVE = 1;
    const INACTIVE = 0;

    public function __construct($name, $description, $image, $category_id, $status = 1, $id = null)
    {
        $this->name = $name;
        $this->description = $description;
        $this->category_id = $category_id;
        $this->image = $image;
        $this->small = BASE_URL . "uploads/small/" . $image;
        $this->thumbnail = BASE_URL . "uploads/thumbnail/" . $image;
        $this->raw = BASE_URL . "uploads/raw/" . $image;
        $this->id = $id;
        $this->status =  $status;
        $categoryRepository = new CategoryRepository();
        $this->category = $categoryRepository->getOne($category_id);
        $vrepository = new VRRepository();
        $this->episodes = $vrepository->getBySeries($id);
    }
}
