<?php

namespace Plinvr\Model;

/**
 * @package Plinvr
 */

class Category
{
    /** properties */

    /** @var int */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $description;

    /** @var int */
    public $status;

    /** @var string */
    public $image;

    /** @var string */
    public $raw;

    /** @var string */
    public $small;
    
    
    const ACTIVE = 1;
    const INACTIVE = 0;

    public function __construct($name, $description, $image,  $status = 1, $id = null)
    {
        $this->name = $name;
        $this->description = $description;
        $this->image = $image;
        $this->small = BASE_URL . "uploads/small/" . $image;
        $this->raw = BASE_URL . "uploads/raw/" . $image;
        $this->id = $id;
        $this->status =  $status;
    }
}
