<?php

namespace Plinvr\Repository;

use Plinvr\Adapter\DatabaseAdapter;
use Plinvr\Model\Series;

/**
 * @package Plinvr
 */

class SeriesRepository extends DatabaseAdapter
{
    public function getOne($id)
    {
        $this->db->where('id', $id);
        $data = $this->db->getOne('series');
        if ($data) {
            return new Series($data['name'], $data['description'],  $data['image'],  $data['category_id'], $data['status'], $data['id']);
        }

        return null;
    }


    public function getAll()
    {
        $result = [];
        $data = $this->db->get('series');
        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new Series($data[$i]['name'], $data[$i]['description'],  $data[$i]['image'], $data[$i]['category_id'],  $data[$i]['status'],  $data[$i]['id']);
        }

        return $result;
    }
    public function getAllActive($start = 0, $limit = 100)
    {
        $result = [];
        $this->db->where("status", 1);
        $this->db->orderBy("id", "DESC");
        $data = $this->db->get('series', [$start, $limit]);
        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new Series($data[$i]['name'], $data[$i]['description'],  $data[$i]['image'], $data[$i]['category_id'],  $data[$i]['status'],  $data[$i]['id']);
        }

        return $result;
    }

    public function getOther($id, $category_id)
    {
        $result = [];
        $this->db->where("status", 1);
        $this->db->where("category_id", $category_id);
        $this->db->where("id <>" . $id);
        $this->db->orderBy("id", "DESC");
        $data = $this->db->get('series', [0, 5]);
        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new Series($data[$i]['name'], $data[$i]['description'],  $data[$i]['image'], $data[$i]['category_id'],  $data[$i]['status'],  $data[$i]['id']);
        }

        return $result;
    }

    public function getlatest()
    {
        $this->db->where("status", 1);
        $this->db->orderBy("id", "DESC");
        $data = $this->db->get('series');
        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            return new Series($data[$i]['name'], $data[$i]['description'],  $data[$i]['image'], $data[$i]['category_id'],  $data[$i]['status'],  $data[$i]['id']);
        }

        return null;
    }

    public function getTotal()
    {
        $data = $this->db->rawQuery('select count(*) tot from series');
        if ($data) {
            return $data[0]['tot'];
        }

        return 0;
    }

    public function getCategoryTotal($category_id)
    {
        $data = $this->db->rawQuery('select count(*) tot from series where  category_id = ' . $category_id);
        if ($data) {
            return $data[0]['tot'];
        }

        return 0;
    }

    public function getByCategory($category_id, $start = 0, $limit = 100)
    {
        $result = [];
        $this->db->where('category_id', $category_id);
        $this->db->where("status", 1);
        $this->db->orderBy("RAND ()");
        $data = $this->db->get('series', [$start, $limit]);
        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new Series($data[$i]['name'], $data[$i]['description'],  $data[$i]['image'], $data[$i]['category_id'],  $data[$i]['status'],  $data[$i]['id']);
        }

        return $result;
    }
}
