<?php

namespace Plinvr\Repository;

use Plinvr\Adapter\DatabaseAdapter;
use Plinvr\Model\Tag;

/**
 * @package Plinvr
 */

class TagRepository extends DatabaseAdapter
{
    public function getOne($id)
    {
        $this->db->where('id', $id);
        $data = $this->db->getOne('`tag`');
        if ($data) {
            return new Tag($data['name'],  $data['status'], $data['id']);
        }

        return null;
    }

    public function getByName($name)
    {
        $this->db->where('name', $name);
        $data = $this->db->getOne('`tag`');
        if ($data) {
            return new Tag($data['name'],  $data['status'], $data['id']);
        }

        return null;
    }

    public function getAll($start = 0, $limit = 100)
    {
        $result = [];
        $this->db->orderBy("name", "ASC");
        $data = $this->db->get('`tag`', [$start, $limit]);

        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new Tag($data[$i]['name'],   $data[$i]['status'],  $data[$i]['id']);
        }

        return $result;
    }
    public function getAllActive($start = 0, $limit = 100)
    {
        $result = [];
        $this->db->where("status", 1);
        $this->db->orderBy("name", "ASC");
        $data = $this->db->get('`tag`', [$start, $limit]);

        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new Tag($data[$i]['name'],   $data[$i]['status'],  $data[$i]['id']);
        }

        return $result;
    }
}
