<?php

namespace Plinvr\Repository;

use Plinvr\Adapter\DatabaseAdapter;
use Plinvr\Model\VRFavourite;

/**
 * @package Plinvr
 */

class VRFavouriteRepository extends DatabaseAdapter
{
    public function getOne($id)
    {
        $this->db->where('id', $id);
        $data = $this->db->getOne('`vr_favourite`');
        if ($data) {
            return new VRFavourite($data['user_id'],  $data['vr_id'], $data['id']);
        }

        return null;
    }

    public function getByUserAndVR($user_id, $vr_id)
    {
        $this->db->where('user_id', $user_id);
        $this->db->where('vr_id', $vr_id);
        $data = $this->db->getOne('`vr_favourite`');
        if ($data) {
            return new VRFavourite($data['user_id'],  $data['vr_id'], $data['id']);
        }

        return null;
    }

    public function getAll($start = 0, $limit = 100)
    {
        $result = [];
        $data = $this->db->get('`vr_favourite`', [$start, $limit]);

        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new VRFavourite( $data[$i]['user_id'],  $data[$i]['vr_id'],    $data[$i]['id']);
        }

        return $result;
    }
    
    public function getAllActive($user_id, $start = 0, $limit = 100)
    {
        $result = [];
        $this->db->where("user_id", $user_id);
        $data = $this->db->get('`vr_favourite`', [$start, $limit]);

        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new VRFavourite( $data[$i]['user_id'],  $data[$i]['vr_id'],    $data[$i]['id']);
        }

        return $result;
    }
}
