<?php

namespace Plinvr\Repository;

use Plinvr\Adapter\DatabaseAdapter;
use Plinvr\Model\VRComment;

/**
 * @package Plinvr
 */

class VRCommentRepository extends DatabaseAdapter
{
    public function getOne($id)
    {
        $this->db->where('id', $id);
        $data = $this->db->getOne('`vr_comment`');
        if ($data) {
            return new VRComment($data['comment'], $data['user_id'],  $data['vr_id'], $data['status'], $data['id'], $data['created_at']);
        }

        return null;
    }

    public function getAll($start = 0, $limit = 100)
    {
        $result = [];
        $this->db->orderBy("comment", "ASC");
        $data = $this->db->get('`vr_comment`', [$start, $limit]);

        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new VRComment($data[$i]['comment'], $data[$i]['user_id'],  $data[$i]['vr_id'],  $data[$i]['status'],  $data[$i]['id']);
        }

        return $result;
    }
    
    public function getAllActive($start = 0, $limit = 100)
    {
        $result = [];
        $this->db->where("status", 1);
        $this->db->orderBy("comment", "ASC");
        $data = $this->db->get('`vr_comment`', [$start, $limit]);

        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new VRComment($data[$i]['comment'], $data[$i]['user_id'],  $data[$i]['vr_id'],  $data[$i]['status'],  $data[$i]['id']);
        }

        return $result;
    }

    public function getVRAllActive($vr_id)
    {
        $result = [];
        $this->db->where("vr_id", $vr_id);
        $this->db->where("status", 1);
        $this->db->orderBy("id", "DESC");
        $data = $this->db->get('`vr_comment`');

        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new VRComment($data[$i]['comment'], $data[$i]['user_id'],  $data[$i]['vr_id'],  $data[$i]['status'],  $data[$i]['id']);
        }

        return $result;
    }
}
