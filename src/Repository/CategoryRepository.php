<?php

namespace Plinvr\Repository;

use Plinvr\Adapter\DatabaseAdapter;
use Plinvr\Model\Category;

/**
 * @package Plinvr
 */

class CategoryRepository extends DatabaseAdapter
{
    public function getOne($id)
    {
        $this->db->where('id', $id);
        $data = $this->db->getOne('`category`');
        if ($data) {
            return new Category($data['name'], $data['description'],  $data['image'], $data['status'], $data['id']);
        }

        return null;
    }

    public function getByName($name)
    {
        $this->db->where('name', $name);
        $data = $this->db->getOne('`category`');
        if ($data) {
            return new Category($data['name'], $data['description'],  $data['image'], $data['status'], $data['id']);
        }

        return null;
    }

    public function getAll($start = 0, $limit = 100)
    {
        $result = [];
        $this->db->orderBy("name", "ASC");
        $data = $this->db->get('`category`', [$start, $limit]);

        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new Category($data[$i]['name'], $data[$i]['description'],  $data[$i]['image'],  $data[$i]['status'],  $data[$i]['id']);
        }

        return $result;
    }
    public function getAllActive($start = 0, $limit = 100)
    {
        $result = [];
        $this->db->where("status", 1);
        $this->db->orderBy("name", "ASC");
        $data = $this->db->get('`category`', [$start, $limit]);

        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new Category($data[$i]['name'], $data[$i]['description'],  $data[$i]['image'],  $data[$i]['status'],  $data[$i]['id']);
        }

        return $result;
    }
}
