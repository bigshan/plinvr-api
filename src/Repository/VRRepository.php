<?php

namespace Plinvr\Repository;

use Plinvr\Adapter\DatabaseAdapter;
use Plinvr\Model\VR;

/**
 * @package Plinvr
 */

class VRRepository extends DatabaseAdapter
{
    public function getOne($id)
    {
        $this->db->where('id', $id);
        $data = $this->db->getOne('vr');
        if ($data) {
            return new VR($data['file'], $data['name'], $data['description'],  $data['image'],  $data['category_id'], $data['location'], $data['is_featured'], $data['coming_soon'], $data['series_id'],  $data['status'], $data['views'], $data['downloaded'], $data['tags'], $data['id'], $data['created_at']);
        }

        return null;
    }

    public function getByName($name)
    {
        $this->db->where('name', $name);
        $data = $this->db->getOne('vr');
        if ($data) {
            return new VR($data['file'], $data['name'], $data['description'],  $data['image'],  $data['category_id'], $data['location'], $data['is_featured'], $data['coming_soon'], $data['series_id'],  $data['status'], $data['views'], $data['downloaded'], $data['tags'], $data['id'], $data['created_at']);
        }

        return null;
    }

    public function getTotal()
    {
        $data = $this->db->rawQuery('select count(*) tot from vr');
        if ($data) {
            return $data[0]['tot'];
        }

        return 0;
    }

    public function getCategoryTotal($category_id)
    {
        $data = $this->db->rawQuery('select count(*) tot from vr where  category_id = ' . $category_id);
        if ($data) {
            return $data[0]['tot'];
        }

        return 0;
    }

    public function getTagTotal($tag)
    {
        $data = $this->db->rawQuery('select count(*) tot from vr where  tags like "%' . $tag . '%"');
        if ($data) {
            return $data[0]['tot'];
        }

        return 0;
    }

    public function getSearchTotal($query)
    {
        $data = $this->db->rawQuery('select count(*) tot from vr where  (name like "%' . $query . '%" or description like  "%' . $query . '%") and status = 1 ');
        if ($data) {
            return $data[0]['tot'];
        }

        return 0;
    }

    public function getAll($start = 0, $limit = 100)
    {
        $result = [];
        $this->db->orderBy("id", "DESC");
        $data = $this->db->get('vr', [$start, $limit]);
        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new VR($data[$i]['file'], $data[$i]['name'], $data[$i]['description'],  $data[$i]['image'], $data[$i]['category_id'], $data[$i]['location'], $data[$i]['is_featured'], $data[$i]['coming_soon'], $data[$i]['series_id'],  $data[$i]['status'], $data[$i]['views'], $data[$i]['downloaded'],  $data[$i]['tags'], $data[$i]['id'], $data[$i]['created_at']);
        }

        return $result;
    }

    public function getAllActive($start = 0, $limit = 100)
    {
        $result = [];
        $this->db->where("status", 1);
        $this->db->orderBy("id", "DESC");
        $data = $this->db->get('vr', [$start, $limit]);
        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new VR($data[$i]['file'], $data[$i]['name'], $data[$i]['description'],  $data[$i]['image'], $data[$i]['category_id'], $data[$i]['location'], $data[$i]['is_featured'], $data[$i]['coming_soon'],  $data[$i]['series_id'],  $data[$i]['status'], $data[$i]['views'], $data[$i]['downloaded'],  $data[$i]['tags'], $data[$i]['id'], $data[$i]['created_at']);
        }

        return $result;
    }

    public function getPopular()
    {
        $result = [];
        $this->db->where("status", 1);
        $this->db->orderBy("views", "DESC");
        $data = $this->db->get('vr', [0, 12]);
        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new VR($data[$i]['file'], $data[$i]['name'], $data[$i]['description'],  $data[$i]['image'], $data[$i]['category_id'], $data[$i]['location'], $data[$i]['is_featured'], $data[$i]['coming_soon'], $data[$i]['series_id'],  $data[$i]['status'], $data[$i]['views'], $data[$i]['downloaded'],  $data[$i]['tags'], $data[$i]['id'], $data[$i]['created_at']);
        }

        return $result;
    }

    public function getFeatured()
    {
        $result = [];
        $this->db->where("is_featured", 1);
        $this->db->where("status", 1);
        $this->db->orderBy("rand()");
        $data = $this->db->get('vr');
        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new VR($data[$i]['file'], $data[$i]['name'], $data[$i]['description'],  $data[$i]['image'], $data[$i]['category_id'], $data[$i]['location'], $data[$i]['is_featured'], $data[$i]['coming_soon'], $data[$i]['series_id'],  $data[$i]['status'], $data[$i]['views'], $data[$i]['downloaded'],  $data[$i]['tags'], $data[$i]['id'], $data[$i]['created_at']);
        }

        return $result;
    }

    public function getComingSoon()
    {
        $this->db->where("coming_soon", 1);
        $this->db->where("status", 1);
        $this->db->orderBy("rand()");
        $data = $this->db->get('vr');
        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            return new VR($data[$i]['file'], $data[$i]['name'], $data[$i]['description'],  $data[$i]['image'], $data[$i]['category_id'], $data[$i]['location'], $data[$i]['is_featured'], $data[$i]['coming_soon'], $data[$i]['series_id'],  $data[$i]['status'], $data[$i]['views'], $data[$i]['downloaded'],  $data[$i]['tags'], $data[$i]['id'], $data[$i]['created_at']);
        }

        return null;
    }

    public function getToday()
    {
        $result = [];
        $this->db->where("status", 1);
        $this->db->where("DATE(`created_at`) = CURDATE()");
        $this->db->orderBy("RAND ()");
        $data = $this->db->get('vr', [0, 6]);
        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new VR($data[$i]['file'], $data[$i]['name'], $data[$i]['description'],  $data[$i]['image'], $data[$i]['category_id'], $data[$i]['location'], $data[$i]['is_featured'], $data[$i]['coming_soon'], $data[$i]['series_id'],  $data[$i]['status'], $data[$i]['views'], $data[$i]['downloaded'],  $data[$i]['tags'], $data[$i]['id'], $data[$i]['created_at']);
        }

        return $result;
    }
    public function getOlder()
    {
        $result = [];
        $this->db->where("status", 1);
        $this->db->where("DATE(`created_at`) < CURDATE()");
        $this->db->orderBy("RAND ()");
        $data = $this->db->get('vr', [0, 6]);
        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new VR($data[$i]['file'], $data[$i]['name'], $data[$i]['description'],  $data[$i]['image'], $data[$i]['category_id'], $data[$i]['location'], $data[$i]['is_featured'], $data[$i]['coming_soon'], $data[$i]['series_id'],  $data[$i]['status'], $data[$i]['views'], $data[$i]['downloaded'],  $data[$i]['tags'], $data[$i]['id'], $data[$i]['created_at']);
        }

        return $result;
    }

    public function getBySeries($series_id)
    {
        $result = [];
        $this->db->where('series_id', $series_id);
        $this->db->orderBy("id", "ASC");
        $data = $this->db->get('vr');
        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new VR($data[$i]['file'], $data[$i]['name'], $data[$i]['description'],  $data[$i]['image'], $data[$i]['category_id'], $data[$i]['location'], $data[$i]['is_featured'], $data[$i]['coming_soon'], $data[$i]['series_id'],  $data[$i]['status'], $data[$i]['views'], $data[$i]['downloaded'],  $data[$i]['tags'], $data[$i]['id'], $data[$i]['created_at']);
        }

        return $result;
    }

    public function getByCategory($category_id, $start = 0, $limit = 100)
    {
        $result = [];
        $this->db->where('category_id', $category_id);
        $this->db->where("status", 1);
        $this->db->orderBy("RAND ()");
        $data = $this->db->get('vr', [$start, $limit]);
        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new VR($data[$i]['file'], $data[$i]['name'], $data[$i]['description'],  $data[$i]['image'], $data[$i]['category_id'], $data[$i]['location'], $data[$i]['is_featured'], $data[$i]['coming_soon'], $data[$i]['series_id'],  $data[$i]['status'], $data[$i]['views'], $data[$i]['downloaded'],  $data[$i]['tags'], $data[$i]['id'], $data[$i]['created_at']);
        }

        return $result;
    }

    public function getByTag($tag, $start = 0, $limit = 100)
    {
        $result = [];
        $this->db->where('tags like "%' . $tag . '%" and status = 1');
        $this->db->orderBy("RAND ()");
        $data = $this->db->get('vr', [$start, $limit]);
        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new VR($data[$i]['file'], $data[$i]['name'], $data[$i]['description'],  $data[$i]['image'], $data[$i]['category_id'], $data[$i]['location'], $data[$i]['is_featured'], $data[$i]['coming_soon'], $data[$i]['series_id'],  $data[$i]['status'], $data[$i]['views'], $data[$i]['downloaded'],  $data[$i]['tags'], $data[$i]['id'], $data[$i]['created_at']);
        }

        return $result;
    }

    public function search($query, $start = 0, $limit = 100)
    {
        $result = [];
        $this->db->where('(name like "%' . $query . '%" or description like  "%' . $query . '%") and status = 1');
        $data = $this->db->get('vr', [$start, $limit]);
        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new VR($data[$i]['file'], $data[$i]['name'], $data[$i]['description'],  $data[$i]['image'], $data[$i]['category_id'], $data[$i]['location'], $data[$i]['is_featured'], $data[$i]['coming_soon'], $data[$i]['series_id'],  $data[$i]['status'], $data[$i]['views'], $data[$i]['downloaded'],  $data[$i]['tags'], $data[$i]['id'], $data[$i]['created_at']);
        }

        return $result;
    }
}
