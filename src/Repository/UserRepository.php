<?php

namespace Plinvr\Repository;

use Plinvr\Adapter\DatabaseAdapter;
use Plinvr\Model\User;

/**
 * @package Plinvr
 */

class UserRepository extends DatabaseAdapter
{
    public function getLogin($email, $password)
    {
        $this->db->where("email", $email);
        $this->db->where("password", $password);

        $data = $this->db->getOne("user");
        if ($data) {
            return new User($data['photo'], $data['lastname'], $data['firstname'],  $data['email'], $data['password'], $data['activation_key'],  $data['forgot_key'], $data['is_activated'], $data['gender'], $data['date_of_birth'], $data['type'], $data['status'], $data['facebook_id'], $data['google_id'], $data['twitter_id'], $data['created_at'],  $data['id']);
        }
        return null;
    }



    public function getById($id)
    {
        $this->db->where("id", $id);
        $data = $this->db->getOne("user");
        if ($data) {
            return new User($data['photo'], $data['lastname'], $data['firstname'],  $data['email'], $data['password'], $data['activation_key'],  $data['forgot_key'], $data['is_activated'], $data['gender'], $data['date_of_birth'], $data['type'], $data['status'], $data['facebook_id'], $data['google_id'], $data['twitter_id'], $data['created_at'],  $data['id']);
        }
        return null;
    }

    public function getByEmail($email)
    {
        $this->db->where("email", $email);
        $data = $this->db->getOne("user");
        if ($data) {
            return new  User($data['photo'], $data['lastname'], $data['firstname'],  $data['email'], $data['password'], $data['activation_key'],  $data['forgot_key'], $data['is_activated'], $data['gender'], $data['date_of_birth'], $data['type'], $data['status'], $data['facebook_id'], $data['google_id'], $data['twitter_id'], $data['created_at'],  $data['id']);
        }
        return null;
    }



    public function getByPlainPassword($password)
    {
        $this->db->where("password", md5($password));
        $data = $this->db->getOne("user");
        if ($data) {
            return new  User($data['photo'], $data['lastname'], $data['firstname'],  $data['email'], $data['password'], $data['activation_key'],  $data['forgot_key'], $data['is_activated'], $data['gender'], $data['date_of_birth'], $data['type'], $data['status'], $data['facebook_id'], $data['google_id'], $data['twitter_id'], $data['created_at'],  $data['id']);
        }
        return null;
    }


    public function getOneByActivateKey($activate_key)
    {
        $this->db->where("activate_key", $activate_key);

        $data = $this->db->getOne("user");
        if ($data) {
            return new  User($data['photo'], $data['lastname'], $data['firstname'],  $data['email'], $data['password'], $data['activation_key'],  $data['forgot_key'], $data['is_activated'], $data['gender'], $data['date_of_birth'], $data['type'], $data['status'], $data['facebook_id'], $data['google_id'], $data['twitter_id'], $data['created_at'],  $data['id']);
        }
        return null;
    }

    public function getAll()
    {
        $result = [];
        $data = $this->db->get('user');
        for (
            $i = 0;
            $i < count($data);
            $i++
        ) {
            $result[] = new User($data[$i]['photo'], $data[$i]['lastname'], $data[$i]['firstname'],  $data[$i]['email'], $data[$i]['password'], $data[$i]['activation_key'],  $data[$i]['forgot_key'], $data[$i]['is_activated'], $data[$i]['gender'], $data[$i]['date_of_birth'], $data[$i]['type'], $data[$i]['status'], $data[$i]['facebook_id'], $data[$i]['google_id'], $data[$i]['twitter_id'], $data[$i]['created_at'],  $data[$i]['id']);
        }

        return $result;
    }
}
