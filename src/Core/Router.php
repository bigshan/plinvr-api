<?php

namespace Plinvr\Core;

class Router
{

    public static function route($url)
    {

        //controller
        if (empty($url[0]))
            return null;

        $controllerRawName = explode("-", $url[0]);
        $controller = "";
        for ($i = 0; $i < count($controllerRawName); $i++) {
            $controller .= ucwords($controllerRawName[$i]);
        }
        $controller =  $controller  . 'Controller';
        array_shift($url);

        //action
        $actionRawName = explode("-", $url[0]);
        $action = "";
        for ($i = 0; $i < count($actionRawName); $i++) {
            if($i > 0)
            $action .= ucwords($actionRawName[$i]);
            else
            $action .= $actionRawName[$i];
        }
        
        $action = !empty($action) ? $action : 'index';
        array_shift($url);

        //file check
        $path =  realpath(dirname(__FILE__)  . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'Controller') . DIRECTORY_SEPARATOR . $controller . '.php';
        if (!file_exists($path)) {
            if (MODE == "development") {
                die('The controller \"' . $controller . '\" does not exist');
            }

            return null;
        }

        //params
        $queryParams = array(implode(",", $url));  
        $controller = '\Plinvr\Controller\\' . $controller;
        $dispatch = new $controller();

        if (method_exists($controller, $action)) {
            echo  call_user_func_array([$dispatch, $action], $queryParams);
        } else {
            if (MODE == "development") {
                die('That method does not exist in the controller \"' . $controller . '\"');
            }
        }
    }
}
