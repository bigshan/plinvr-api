<?php

namespace Plinvr\Controller;

/**
 * @package Plinvr
 */

use Plinvr\Manager\CategoryManager;
use Plinvr\Model\Category;
use Plinvr\Repository\CategoryRepository;
use Dtkahl\FlashMessages\FlashMessages;
use Gumlet\ImageResize;

class CategoryController
{
    /** @var CategoryManager */
    private $manager;

    /** @var CategoryRepository */
    private $repository;

    /** @var FlashMessages */
    private $flash;

    public function __construct()
    {
        $this->manager = new CategoryManager();
        $this->repository = new CategoryRepository();
        $this->flash = new FlashMessages();
    }

    public function add()
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }

        $name = $_POST['name'];
        $description = $_POST['description'];
        $status = $_POST['status'];

        if (empty($name)) {
            return json_encode(["success" => false, "message" => "Task failed because category name is empty", "data" => null]);
        }

        $target_dir = "uploads/raw/";
        if (isset($_FILES["image"])) {

            $imageFileType = strtolower(pathinfo($target_dir . $_FILES["image"]["name"], PATHINFO_EXTENSION));
            $generatedFileName = "category_" . time() . "." . $imageFileType;
            $target_raw = $target_dir . $generatedFileName;

            // Check if image file is a actual image or fake image
            if (!($imageFileType == 'jpg' || $imageFileType == 'png' || $imageFileType == 'gif')) {
                $this->flash->setError("message", "The file must be Image");
                header("location: " . BASE_URL . "admin/new-category");
                exit;
            }

            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_raw)) {
                $image = new ImageResize(getcwd() . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . "raw" . DIRECTORY_SEPARATOR . $generatedFileName);
                $image->crop(300, 200, true, ImageResize::CROPCENTER);
                $image->save("uploads/thumbnail/" . $generatedFileName);
                $image->crop(204, 120, true, ImageResize::CROPCENTER);
                $image->save("uploads/small/" . $generatedFileName);
            } else {
                $this->flash->setError("message",  "Sorry, there was an error uploading your thymbnail.");
                header("location: " . BASE_URL . "admin/new-category");
                exit;
            }
        }
        $category = new Category($name, $description, $generatedFileName, $status);

        $result = $this->manager->add($category);
        header("location: " . BASE_URL . "admin/categories");
    }

    public function update()
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }

        $name = $_POST['name'];
        $description = $_POST['description'];
        $status = $_POST['status'];
        $id = $_POST['id'];

        if (empty($id) || empty($name)) {
            return json_encode(["success" => false, "message" => "Task failed because one of the parameter is empty", "data" => null]);
        }

        $category = $this->repository->getOne($id);
        $target_dir = "uploads/raw/";

        if ($_FILES["image"]["name"]) {
            $imageFileType = strtolower(pathinfo($target_dir . $_FILES["image"]["name"], PATHINFO_EXTENSION));
            $generatedFileName = "vr_" . time() . "." . $imageFileType;
            $target_raw = $target_dir . $generatedFileName;

            // Check if image file is a actual image or fake image
            if (!($imageFileType == 'jpg' || $imageFileType == 'png' || $imageFileType == 'gif')) {
            }

            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_raw)) {
                $image = new ImageResize(getcwd() . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . "raw" . DIRECTORY_SEPARATOR . $generatedFileName);
                $image->crop(300, 500, true, ImageResize::CROPCENTER);
                $image->save("uploads/thumbnail/" . $generatedFileName);
                $image->crop(204, 120, true, ImageResize::CROPCENTER);
                $image->save("uploads/small/" . $generatedFileName);
                $category->image = $generatedFileName;
            }
        }
        $category->name = $name;
        $category->description = $description;
        $category->status = $status;
        $result = $this->manager->update($category);
        header("location: " . BASE_URL . "admin/categories");
    }

    public function remove($id)
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }

        if (empty($id)) {
            $this->flash->setError("message",  "Task failed because one of the parameter is empty");
            header("location: " . BASE_URL . "admin/categories");
            exit;
        }

        $result = $this->manager->delete($id);
        header("location: " . BASE_URL . "admin/categories");
    }

    public function all($params)
    {
        $params =  explode('/', $params);
        $data = count($params) > 1 ? $this->repository->getAllActive($params[0], $params[1]) : $this->repository->getAllActive();
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function one($id)
    {
        $data = $this->repository->getOne($id);
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function name($name)
    {
        $data = $this->repository->getByName(str_replace("%20", " ", $name));
        return json_encode(["code" => 1, "data" => $data]);
    }
}
