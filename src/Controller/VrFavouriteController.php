<?php

namespace Plinvr\Controller;

/**
 * @package Plinvr
 */

use Plinvr\Manager\VRFavouriteManager;
use Plinvr\Model\VRFavourite;
use Plinvr\Repository\VRFavouriteRepository;

class VRFavouriteController
{
    /** @var VRFavouriteManager */
    private $manager;

    /** @var VRFavouriteRepository */
    private $repository;


    public function __construct()
    {
        $this->manager = new VRFavouriteManager();
        $this->repository = new VRFavouriteRepository();
    }

    public function add()
    {
        $data = json_decode(file_get_contents("php://input"));
        $user_id = $data->user_id;
        $vr_id =  $data->vr_id;
        $vr_favourite = new VRFavourite($user_id, $vr_id);
        $result = $this->manager->add($vr_favourite);

        return json_encode(["code" => 1, "data" => $result]);
    }


    public function remove()
    {
        $data = json_decode(file_get_contents("php://input"));
        $user_id = $data->user_id;
        $vr_id =  $data->vr_id;
        $vr_favourite = $this->repository->getByUserAndVR($user_id, $vr_id);
        $this->manager->delete($vr_favourite->id);
        return json_encode(["code" => 1, "data" =>  $vr_favourite]);
    }

    public function all($params)
    {
        $params =  explode('/', $params);
        $data = count($params) > 1 ? $this->repository->getAllActive($params[0], $params[1], $params[2]) : $this->repository->getAllActive($params[0]);
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function one($params)
    {
        $params =  explode(',', $params);
        $data = $this->repository->getByUserAndVR($params[0], $params[1]);
        return json_encode(["code" => 1, "data" => $data]);
    }
}
