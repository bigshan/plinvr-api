<?php

namespace Plinvr\Controller;

/**
 * @package Plinvr
 */

use Plinvr\Manager\TagManager;
use Plinvr\Model\Tag;
use Plinvr\Repository\TagRepository;
use Dtkahl\FlashMessages\FlashMessages;

class TagController
{
    /** @var TagManager */
    private $manager;

    /** @var TagRepository */
    private $repository;

    /** @var FlashMessages */
    private $flash;

    public function __construct()
    {
        $this->manager = new TagManager();
        $this->repository = new TagRepository();
        $this->flash = new FlashMessages();
    }

    public function add()
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }

        $name = $_POST['name'];
        $status = $_POST['status'];

        if (empty($name)) {
            return json_encode(["success" => false, "message" => "Task failed because tag name is empty", "data" => null]);
        }

        
        $tag = new Tag($name, $status);

        $result = $this->manager->add($tag);
        header("location: " . BASE_URL . "admin/tags");
    }

    public function update()
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }

        $name = $_POST['name'];
       $status = $_POST['status'];
        $id = $_POST['id'];

        if (empty($id) || empty($name)) {
            return json_encode(["success" => false, "message" => "Task failed because one of the parameter is empty", "data" => null]);
        }

        $tag = $this->repository->getOne($id);
        
        $tag->name = $name;
        $tag->status = $status;
        $result = $this->manager->update($tag);
        header("location: " . BASE_URL . "admin/tags");
    }

    public function remove($id)
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }

        if (empty($id)) {
            $this->flash->setError("message",  "Task failed because one of the parameter is empty");
            header("location: " . BASE_URL . "admin/tags");
            exit;
        }

        $result = $this->manager->delete($id);
        header("location: " . BASE_URL . "admin/tags");
    }

    public function all($params)
    {
        $params =  explode('/', $params);
        $data = count($params) > 1 ? $this->repository->getAllActive($params[0], $params[1]) : $this->repository->getAllActive();
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function one($id)
    {
        $data = $this->repository->getOne($id);
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function name($name)
    {
        $data = $this->repository->getByName(str_replace("%20", " ", $name));
        return json_encode(["code" => 1, "data" => $data]);
    }
}
