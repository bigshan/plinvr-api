<?php

namespace Plinvr\Controller;

/**
 * @package Plinvr
 */

use Plinvr\Manager\SeriesManager;
use Plinvr\Model\Series;
use Plinvr\Repository\SeriesRepository;
use Dtkahl\FlashMessages\FlashMessages;
use Gumlet\ImageResize;

class SeriesController
{
    /** @var SeriesManager */
    private $manager;

    /** @var SeriesRepository */
    private $repository;

    /** @var FlashMessages */
    private $flash;

    public function __construct()
    {
        $this->manager = new SeriesManager();
        $this->repository = new SeriesRepository();
        $this->flash = new FlashMessages();
    }

    public function add()
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }

        $name = $_POST['name'];
        $description = $_POST['description'];
        $category_id = $_POST['category_id'];
        $status = $_POST['status'];

        if (empty($name)) {
            return json_encode(["success" => false, "message" => "Task failed because series name is empty", "data" => null]);
        }

        $target_dir = "uploads/raw/";
        if (isset($_FILES["image"])) {

            $imageFileType = strtolower(pathinfo($target_dir . $_FILES["image"]["name"], PATHINFO_EXTENSION));
            $generatedFileName = "series_" . time() . "." . $imageFileType;
            $target_raw = $target_dir . $generatedFileName;

            // Check if image file is a actual image or fake image
            if (!($imageFileType == 'jpg' || $imageFileType == 'png' || $imageFileType == 'gif')) {
                $this->flash->setError("message", "The file must be Image");
                header("location: " . BASE_URL . "admin/new-series");
                exit;
            }

            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_raw)) {
                $image = new ImageResize(getcwd() . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . "raw" . DIRECTORY_SEPARATOR . $generatedFileName);
                $image->crop(300, 200, true, ImageResize::CROPCENTER);
                $image->save("uploads/thumbnail/" . $generatedFileName);
                $image->crop(204, 120, true, ImageResize::CROPCENTER);
                $image->save("uploads/small/" . $generatedFileName);
            } else {
                $this->flash->setError("message",  "Sorry, there was an error uploading your thymbnail.");
                header("location: " . BASE_URL . "admin/new-series");
                exit;
            }
        }
        $series = new Series($name, $description, $generatedFileName, $category_id, $status);

        $result = $this->manager->add($series);
        header("location: " . BASE_URL . "admin/all-series");
    }

    public function update()
    {
        $name = $_POST['name'];
        $description = $_POST['description'];
        $status = $_POST['status'];
        $category_id = $_POST['category_id'];
        $id = $_POST['id'];

        if (empty($id) || empty($name)) {
            return json_encode(["success" => false, "message" => "Task failed because one of the parameter is empty", "data" => null]);
        }

        $series = $this->repository->getOne($id);
        $target_dir = "uploads/raw/";
        if ($_FILES["image"]["name"]) {

            $imageFileType = strtolower(pathinfo($target_dir . $_FILES["image"]["name"], PATHINFO_EXTENSION));
            $generatedFileName = "series_" . time() . "." . $imageFileType;
            $target_raw = $target_dir . $generatedFileName;

            // Check if image file is a actual image or fake image
            if (!($imageFileType == 'jpg' || $imageFileType == 'png' || $imageFileType == 'gif')) {
                $this->flash->setError("message", "The file must be Image");
                header("location: " . BASE_URL . "admin/new-series");
                exit;
            }

            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_raw)) {
                $image = new ImageResize(getcwd() . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . "raw" . DIRECTORY_SEPARATOR . $generatedFileName);
                $image->crop(300, 200, true, ImageResize::CROPCENTER);
                $image->save("uploads/thumbnail/" . $generatedFileName);
                $image->crop(204, 120, true, ImageResize::CROPCENTER);
                $image->save("uploads/small/" . $generatedFileName);
                $series->image = $generatedFileName;
            } else {
                $this->flash->setError("message",  "Sorry, there was an error uploading your thymbnail.");
                header("location: " . BASE_URL . "admin/new-series");
                exit;
            }
        }
        $series->name = $name;
        $series->description = $description;
        $series->status = $status;
        $series->category_id = $category_id;
        $result = $this->manager->update($series);

        header("location: " . BASE_URL . "admin/all-series");
    }

    public function remove()
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }

        if (empty($id)) {
            $this->flash->setError("message",  "Task failed because one of the parameter is empty");
            header("location: " . BASE_URL . "admin/all-series");
            exit;
        }

        $result = $this->manager->delete($id);
        header("location: " . BASE_URL . "admin/all-series");
    }

    public function totalCategory($category_id)
    {
        $data = $this->repository->getCategoryTotal($category_id);
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function total()
    {
        $data = $this->repository->getTotal();
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function all($params)
    {
        $params =  explode(',', $params);
        $data = count($params) > 1 ? $this->repository->getAllActive($params[0], $params[1]) : $this->repository->getAllActive();
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function latest()
    {
        $data = $this->repository->getlatest();
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function one($id)
    {
        $data = $this->repository->getOne($id);
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function other($params)
    {
        $params =  explode(',', $params);
        $data = $this->repository->getOther($params[0], $params[1]);
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function category($params)
    {
        $params =  explode(',', $params);
        $data = $this->repository->getByCategory($params[0], $params[1], $params[2]);
        return json_encode(["code" => 1, "data" => $data]);
    }
}
