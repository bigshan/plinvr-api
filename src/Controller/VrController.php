<?php

namespace Plinvr\Controller;

/**
 * @package Plinvr
 */

use Dtkahl\FlashMessages\FlashMessages;
use Plinvr\Manager\VRManager;
use Plinvr\Model\VR;
use Plinvr\Repository\VRRepository;
use Gumlet\ImageResize;

class VRController
{
    /** @var VRManager */
    private $manager;

    /** @var VRRepository */
    private $repository;

    /** @var FlashMessages */
    private $flash;

    public function __construct()
    {
        $this->manager = new VRManager();
        $this->repository = new VRRepository();
        $this->flash = new FlashMessages();
    }

    public function add()
    {
        $name = $_POST['name'];
        $description = $_POST['description'];
        $series_id = $_POST['series_id'];
        $category_id = $_POST['category_id'];
        $status = $_POST['status'];
        $location = $_POST['location'];
        $is_featured = $_POST['is_featured'];
        $coming_soon = $_POST['coming_soon'];
        $tags = $_POST['tags'];
        if (empty($name)) {
            return json_encode(["success" => false, "message" => "Task failed because vr name is empty", "data" => null]);
        }

        $target_dir = "uploads/raw/";
        if (isset($_FILES["image"])) {

            $imageFileType = strtolower(pathinfo($target_dir . $_FILES["image"]["name"], PATHINFO_EXTENSION));
            $generatedFileName = "vr_" . time() . "." . $imageFileType;
            $target_raw = $target_dir . $generatedFileName;

            // Check if image file is a actual image or fake image
            if (!($imageFileType == 'jpg' || $imageFileType == 'png' || $imageFileType == 'gif')) {
                $this->flash->setError("message", "The file must be Image");
                if ($series_id > 0) {
                    header("location: " . BASE_URL . "admin/series/" . $series_id);
                } else {
                    header("location: " . BASE_URL . "admin/new-category");
                }
                exit;
            }

            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_raw)) {
                $image = new ImageResize(getcwd() . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . "raw" . DIRECTORY_SEPARATOR . $generatedFileName);
                $image->crop(300, 500, true, ImageResize::CROPCENTER);
                $image->save("uploads/thumbnail/" . $generatedFileName);
                $image->crop(204, 120, true, ImageResize::CROPCENTER);
                $image->save("uploads/small/" . $generatedFileName);
            } else {
                $this->flash->setError("message",  "Sorry, there was an error uploading your thymbnail.");
                if ($series_id > 0) {
                    header("location: " . BASE_URL . "admin/series/" . $series_id);
                } else {
                    header("location: " . BASE_URL . "admin/new-category");
                }
                exit;
            }
        }

        $target_dir = "uploads/vr/";

        if (isset($_FILES["file"])) {

            $imageFileType = strtolower(pathinfo($target_dir . $_FILES["file"]["name"], PATHINFO_EXTENSION));
            $original_name  = str_replace(".zip", "", $_FILES["file"]["name"]);
            $filename =  "vr_" . time();
            $target_file =  $target_dir . $filename . "." . $imageFileType;

            // Check if image file is a actual image or fake image
            if (!($imageFileType == 'zip')) {
                $this->flash->setError("message", "The file must be zip file");
                if ($series_id > 0) {
                    header("location: " . BASE_URL . "admin/series/" . $series_id);
                } else {
                    header("location: " . BASE_URL . "admin/new-category");
                }
                exit;
            }

            if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
                if (!file_exists($target_dir . $filename)) {
                    mkdir($target_dir . $filename, 0777, true);
                }

                $extractPath = getcwd() . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . "vr" . DIRECTORY_SEPARATOR . $filename;
                $zip = new \ZipArchive;
                $res = $zip->open($target_file);
                if ($res === TRUE) {
                    $zip->extractTo($extractPath);
                    $zip->close();
                    $vr = new VR($filename . "/" + $original_name + "/", $name, $description, $generatedFileName, $category_id, $location, $is_featured, $coming_soon, $series_id, $status,  0, 0, $tags);
                    $result = $this->manager->add($vr);

                    if ($series_id > 0) {
                        header("location: " . BASE_URL . "admin/series/" . $series_id);
                    } else {
                        header("location: " . BASE_URL . "admin/vrs");
                    }
                    exit;
                } else {
                    if ($series_id > 0) {
                        header("location: " . BASE_URL . "admin/series/" . $series_id);
                    } else {
                        header("location: " . BASE_URL . "admin/new-category");
                    }
                    exit;
                }
            } else {
                $this->flash->setError("message",  "Sorry, there was an error uploading your file.");
                if ($series_id > 0) {
                    header("location: " . BASE_URL . "admin/series/" . $series_id);
                } else {
                    header("location: " . BASE_URL . "admin/new-category");
                }
                exit;
            }
        } else {
            $this->flash->setError("message",  "Sorry, there was no file.");
            if ($series_id > 0) {
                header("location: " . BASE_URL . "admin/series/" . $series_id);
            } else {
                header("location: " . BASE_URL . "admin/new-category");
            }
            exit;
        }
    }

    public function update()
    {
        $name = $_POST['name'];
        $description = $_POST['description'];
        $image = $_POST['image'];
        $status = $_POST['status'];
        $category_id = $_POST['category_id'];
        $location = $_POST['location'];
        $is_featured = $_POST['is_featured'];
        $coming_soon = $_POST['coming_soon'];
        $tags = $_POST['tags'];
        $id = $_POST['id'];

        if (empty($id) || empty($name)) {
            return json_encode(["success" => false, "message" => "Task failed because one of the parameter is empty", "data" => null]);
        }

        $vr = $this->repository->getOne($id);

        $target_dir = "uploads/raw/";

        if ($_FILES["image"]["name"]) {

            $imageFileType = strtolower(pathinfo($target_dir . $_FILES["image"]["name"], PATHINFO_EXTENSION));
            $generatedFileName = "vr_" . time() . "." . $imageFileType;
            $target_raw = $target_dir . $generatedFileName;

            // Check if image file is a actual image or fake image
            if (!($imageFileType == 'jpg' || $imageFileType == 'png' || $imageFileType == 'gif')) {
            }

            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_raw)) {
                $image = new ImageResize(getcwd() . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . "raw" . DIRECTORY_SEPARATOR . $generatedFileName);
                $image->crop(300, 500, true, ImageResize::CROPCENTER);
                $image->save("uploads/thumbnail/" . $generatedFileName);
                $image->crop(204, 120, true, ImageResize::CROPCENTER);
                $image->save("uploads/small/" . $generatedFileName);
                $vr->image = $generatedFileName;
            }
        }

        $target_dir = "uploads/vr/";

        if ($_FILES["file"]["name"]) {
            $original_name  = str_replace(".zip", "", $_FILES["file"]["name"]);
            $imageFileType = strtolower(pathinfo($target_dir . $_FILES["file"]["name"], PATHINFO_EXTENSION));
            $filename =  "vr_" . time();
            $target_file =  $target_dir . $filename . "." . $imageFileType;

            // Check if image file is a actual image or fake image
            if (!($imageFileType == 'zip')) {
                $this->flash->setError("message", "The file must be zip file");
                header("location: " . BASE_URL . "admin/new-vr/" . $id);
                exit;
            }

            if (move_uploaded_file($_FILES["file"]["tmp_name"], $target_file)) {
                if (!file_exists($target_dir . $filename)) {
                    mkdir($target_dir . $filename, 0777, true);
                }

                $extractPath = getcwd() . DIRECTORY_SEPARATOR . "uploads" . DIRECTORY_SEPARATOR . "vr" . DIRECTORY_SEPARATOR . $filename;
                $zip = new \ZipArchive;
                $res = $zip->open($target_file);
                if ($res === TRUE) {
                    $zip->extractTo($extractPath);
                    $zip->close();
                    $vr->file = $filename . "/" . $original_name . "/";
                }
            }
        }


        $vr->name = $name;
        $vr->description = $description;
        $vr->status = $status;
        $vr->category_id = $category_id;
        $vr->location = $location;
        $vr->is_featured = $is_featured;
        $vr->tags = $tags;
        $vr->coming_soon = $coming_soon;
        $result = $this->manager->update($vr);
        header("location: " . BASE_URL . "admin/vrs");
    }

    public function view($id)
    {

        if (empty($id)) {
            return json_encode(["success" => false, "message" => "Task failed because idr is empty", "data" => null]);
        }

        $vr = $this->repository->getOne($id);
        $vr->views = intval($vr->views) + 1;
        $result = $this->manager->update($vr);
        return json_encode(["code" => 1, "data" => $result]);
    }

    public function remove($id)
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }

        if (empty($id)) {
            $this->flash->setError("message",  "Task failed because one of the parameter is empty");
            header("location: " . BASE_URL . "admin/vrs");
            exit;
        }

        $result = $this->manager->delete($id);
        header("location: " . BASE_URL . "admin/vrs");
    }

    public function all($params)
    {
        $params =  explode(',', $params);
        $data = count($params) > 1 ? $this->repository->getAllActive($params[0], $params[1]) : $this->repository->getAllActive();
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function one($id)
    {
        $data = $this->repository->getOne($id);
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function category($params)
    {
        $params =  explode(',', $params);
        $data = $this->repository->getByCategory($params[0], $params[1], $params[2]);
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function tag($params)
    {
        $params =  explode(',', $params);
        $data = $this->repository->getByTag($params[0], $params[1], $params[2]);
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function search($params)
    {
        $params =  explode(',', $params);
        $data = $this->repository->search(str_replace("+", " ", $params[0]), $params[1], $params[2]);
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function popular()
    {
        $data = $this->repository->getPopular();
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function featured()
    {
        $data = $this->repository->getFeatured();
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function total()
    {
        $data = $this->repository->getTotal();
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function totalCategory($category_id)
    {
        $data = $this->repository->getCategoryTotal($category_id);
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function totalTag($tag)
    {
        $data = $this->repository->getTagTotal($tag);
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function totalSearch($query)
    {
        $data = $this->repository->getSearchTotal($query);
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function comingSoon()
    {
        $data = $this->repository->getComingSoon();
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function today()
    {
        $data = $this->repository->getToday();
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function older()
    {
        $data = $this->repository->getOlder();
        return json_encode(["code" => 1, "data" => $data]);
    }
}
