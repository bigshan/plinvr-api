<?php

namespace Plinvr\Controller;

/**
 * @package Plinvr
 */

use Plinvr\Manager\VRCommentManager;
use Plinvr\Model\VRComment;
use Plinvr\Repository\VRCommentRepository;
use Dtkahl\FlashMessages\FlashMessages;

class VRCommentController
{
    /** @var VRCommentManager */
    private $manager;

    /** @var VRCommentRepository */
    private $repository;

    /** @var FlashMessages */
    private $flash;

    public function __construct()
    {
        $this->manager = new VRCommentManager();
        $this->repository = new VRCommentRepository();
        $this->flash = new FlashMessages();
    }

    public function add()
    {
        $data = json_decode(file_get_contents("php://input"));
        $comment = $data->comment;
        $user_id = $data->user_id;
        $vr_id = $data->vr_id;
        $status = $data->status;

        if (empty($comment)) {
            return json_encode(["success" => false, "message" => "Task failed because  comment is empty", "data" => null]);
        }

        $vr_comment = new VRComment($comment, $user_id, $vr_id, $status);

        $result = $this->manager->add($vr_comment);
        return json_encode(["code" => 1, "data" => $result]);
    }

    public function update()
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }
        $data = json_decode(file_get_contents("php://input"));
        $comment = $data->comment;
        $status = $data->status;
        $id = $data->id;

        if (empty($id) || empty($comment)) {
            return json_encode(["success" => false, "message" => "Task failed because one of the parameter is empty", "data" => null]);
        }

        $vr_comment = $this->repository->getOne($id);

        $vr_comment->comment = $comment;
        $vr_comment->status = $status;
        $result = $this->manager->update($vr_comment);
        header("location: " . BASE_URL . "admin/comments");
    }

    public function remove($id)
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }

        if (empty($id)) {
            $this->flash->setError("message",  "Task failed because one of the parameter is empty");
            header("location: " . BASE_URL . "admin/comments");
            exit;
        }

        $result = $this->manager->delete($id);
        header("location: " . BASE_URL . "admin/comments");
    }

    public function all($params)
    {
        $params =  explode('/', $params);
        $data = count($params) > 1 ? $this->repository->getAllActive($params[0], $params[1]) : $this->repository->getAllActive();
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function one($id)
    {
        $data = $this->repository->getOne($id);
        return json_encode(["code" => 1, "data" => $data]);
    }

    public function vr($vr_id)
    {
        $data = $this->repository->getVRAllActive($vr_id);
        return json_encode(["code" => 1, "data" => $data]);
    }
}
