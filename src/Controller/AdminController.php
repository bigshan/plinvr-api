<?php

namespace Plinvr\Controller;

use Dtkahl\FlashMessages\FlashMessages;
use Plinvr\Repository\CategoryRepository;
use Plinvr\Repository\SeriesRepository;
use Plinvr\Repository\UserRepository;
use Plinvr\Repository\VRRepository;
use Plinvr\Repository\VRCommentRepository;
use Plinvr\Repository\TagRepository;

/**
 * @package Plinvr
 */

class AdminController
{

    /** @var FlashMessages */
    private $flash;

    public function __construct()
    {
        $this->flash = new FlashMessages();
    }

    public function login()
    {
        if (isset($_REQUEST['username'])) {
            $username = $_REQUEST['username'];
            $password = $_REQUEST['password'];

            if (empty($username) || empty($password)) {
                $this->flash->setError("message", "Username or Password can not be empty");
                $message =   $this->flash->getError('message');
                header("location: " . BASE_URL . "admin/login");
                exit;
            }

            if (strtolower($username)  !=  "admin@plinvr.com" || $password != "Ice.Bifolu?123@") {

                $this->flash->setError("message", "Invalid  Login Credentials");
                $message =   $this->flash->getError('message');
                header("location: " . BASE_URL . "admin/login");
                exit;
            }


            $_SESSION['user'] = 1;
            $message =   $this->flash->getError('message');

            header("location: " . BASE_URL . "admin/categories");
            exit;
        }
        $message =   $this->flash->getError('message');
        include 'templates/sign-in.php';
    }

    public  function categories()
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }

        $categoryRepository = new CategoryRepository();
        $categories =  $categoryRepository->getAll();
        $message =   $this->flash->getError('message');
        include 'templates/category-list.php';
    }

    public  function tags()
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }

        $tagRepository = new TagRepository();
        $tags =  $tagRepository->getAll();
        $message =   $this->flash->getError('message');
        include 'templates/tag-list.php';
    }

    public  function users()
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }

        $userRepository = new UserRepository();
        $users =  $userRepository->getAll();
        $message =   $this->flash->getError('message');
        include 'templates/user-list.php';
    }

    public  function vrs()
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }

        $vrRepository = new VRRepository();
        $vrs =  $vrRepository->getAll();
        $message =   $this->flash->getError('message');
        include 'templates/vr-list.php';
    }
    public  function comments()
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }

        $vrCommentRepository = new VRCommentRepository();
        $comments =  $vrCommentRepository->getAll();
        $message =   $this->flash->getError('message');
        include 'templates/comment-list.php';
    }

    public  function allSeries()
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }

        $seriesRepository = new SeriesRepository();
        $allSeries =  $seriesRepository->getAll();
        $message =   $this->flash->getError('message');
        include 'templates/series-list.php';
    }

    public  function series($id)
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }

        $vrRepository = new VRRepository();
        $vrs =  $vrRepository->getBySeries($id);
        $seriesRepository = new SeriesRepository();
        $series =  $seriesRepository->getOne($id);
        $categoryRepository = new CategoryRepository();
        $categories =  $categoryRepository->getAll();
        $message =   $this->flash->getError('message');
        include 'templates/series.php';
    }

    public  function newCategory($id)
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }
        if (intval($id) > 0) {
            $categoryRepository = new CategoryRepository();
            $category =  $categoryRepository->getOne($id);
        }
        $message =   $this->flash->getError('message');
        include 'templates/add-category.php';
    }

    public  function newTag($id)
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }
        if (intval($id) > 0) {
            $tagRepository = new TagRepository();
            $tag =  $tagRepository->getOne($id);
        }
        $message =   $this->flash->getError('message');
        include 'templates/add-tag.php';
    }

    public  function newSeries($id)
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }
        if (intval($id) > 0) {
            $seriesRepository = new SeriesRepository();
            $series =  $seriesRepository->getOne($id);
        }

        $categoryRepository = new CategoryRepository();
        $categories =  $categoryRepository->getAll();
        $message =   $this->flash->getError('message');
        include 'templates/add-series.php';
    }

    public  function newVr($id)
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }
        if (intval($id) > 0) {
            $vrRepository = new VRRepository();
            $vr =  $vrRepository->getOne($id);
        }

        $categoryRepository = new CategoryRepository();
        $categories =  $categoryRepository->getAll();
        $tagRepository = new TagRepository();
        $tags =  $tagRepository->getAll();
        $message =   $this->flash->getError('message');
        include 'templates/add-vr.php';
    }
}
