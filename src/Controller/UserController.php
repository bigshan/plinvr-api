<?php

namespace Plinvr\Controller;

/**
 * @package Plinvr
 */

use Plinvr\Manager\UserManager;
use Plinvr\Model\User;
use Plinvr\Repository\UserRepository;
use Plinvr\Messager\EmailMessager;

class UserController
{

    /** @var UserRepository */
    private $repository;

    /** @var UserManager */
    private $manager;

    /** @var EmailMessager */
    private $emailMessagers;

    public function __construct()
    {
        $this->repository = new UserRepository();
        $this->manager = new UserManager();
        $this->emailMessagers = new EmailMessager();
    }

    public function index()
    {
        return  null;
    }

    public function register()
    {
        $data = json_decode(file_get_contents("php://input"));
        $lastname = $data->lastname;
        $firstname = $data->firstname;
        $email = $data->email;
        $password = $data->password;

        if (empty($lastname) || empty($firstname) || empty($email) || empty($password)) {
            return json_encode(['success' => false, 'message' => 'user is empty', 'data' => null]);
        }
        $user = new User(null, $lastname, $firstname, $email,  md5($password), $this->generate());
        $result = $this->manager->add($user);
        return $result ? json_encode(['code' => 1, 'message' => '', 'data' => $user]) : json_encode(['success' => false, 'message' => 'Task failed', 'data' => null]);
    }

    public function login()
    {
        $data = json_decode(file_get_contents("php://input"));
        $email = $data->email;
        $password = $data->password;

        if (empty($email) || empty($password)) {
            return json_encode(['code' => 0, 'message' => 'Task failed because one of the parameter is empty', 'data' => null]);
        }

        $data = $this->repository->getLogin($email, md5($password));
        if ($data) {
            echo json_encode(['code' => 1, 'data' => $data]);
        } else {
            echo json_encode(['code' => 0, 'message' => 'Failed to authenticate']);
        }
    }

    private function generate($strength = 16)
    {
        $input = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $input_length = strlen($input);
        $random_string = '';
        for ($i = 0; $i < $strength; $i++) {
            $random_character = $input[mt_rand(0, $input_length - 1)];
            $random_string .= $random_character;
        }

        return $random_string . "_" . md5(time());
    }

    private function forgotPassworMailTemplate($user, $activate_key)
    {
        $message = "Hi,  <br/>      

        <p>Please click the link below to reset your password. </p>

        <p> </p>

        If you have any questions or concerns, please feel free to reach out to <a>info@plinvr.com</a>.
        <br/>
        Thank you and happy scripting!
        -PLINVR Team";

        return $message;
    }

    private function changedPassworMailTemplate($user)
    {
        $message = "Hi,  <br/>      

        <p>You have succesfully changed your password</p>

        If you have any questions or concerns, please feel free to reach out to <a>info@plinvr.com</a>.
        <br/>
        Thank you and happy scripting!
        -PLINVR Team";

        return $message;
    }


    public function forgotPassword()
    {
        $email = $_REQUEST['email'];

        if (empty($email)) {
            echo json_encode(['success' => false, 'message' => 'Email is empty', 'data' => null]);
        }

        $data = $this->repository->getByEmail($email);
        if ($data && $data->is_activated == 1) {
            $activate_key = $this->generate();
            $this->emailMessagers->send("Password Reset ", $this->forgotPassworMailTemplate($data,  $activate_key), MAIL_USERNAME, $email, $email);
            $data->password = $activate_key;
            $result = $this->manager->update($data);
            echo json_encode(['success' => true, 'message' => 'We have sent you instruction on how to reset your password, please check your mail', 'data' => $data]);
        } else {
            echo json_encode(['success' => false, 'message' => 'No account associate to this email', 'data' => null]);
        }
    }

    public function changePassword()
    {
        $new_password = $_REQUEST['password'];
        $old_password = $_REQUEST['old_password'];
        if (empty($old_password) || empty($new_password)) {
            echo json_encode(['success' => false, 'message' => 'Password is empty', 'data' => null]);
        }

        $user = $this->repository->getByPlainPassword($old_password);

        if (!$user) {
            echo json_encode(['success' => false, 'message' => 'Failed to change password, Old password is not correct', 'data' => null]);
            return;
        }

        if ($user && $user->is_activated == 1) {
            $user->password = md5($new_password);
            $this->manager->update($user);
            echo json_encode(['success' => true, 'message' => 'Password changed', 'data' => $user]);
            $this->emailMessagers->send("Password Changed ", $this->changedPassworMailTemplate($user), MAIL_USERNAME, $user->email, $user->email);
        } else {
            echo json_encode(['success' => false, 'message' => 'Failed to change password', 'data' => null]);
        }
    }

    public function changeStatus($params)
    {
        if (!isset($_SESSION['user']) || $_SESSION['user'] != 1) {
            $this->flash->setError("message",  "Sorry, Unauthorized Access");
            header("location: " . BASE_URL . "admin/login");
        }

        $params =  explode(',', $params);

        $user = $this->repository->getById($params[0]);

        if (!$user) {
            $this->flash->setError("message",  "No User Found");
            header("location: " . BASE_URL . "admin/users");
            exit;
        }

        $user->status = $params[1];
        $this->manager->update($user);
        header("location: " . BASE_URL . "admin/users");
    }



    public function activate($activate_key)
    {
        $user = $this->repository->getOneByActivateKey($activate_key);
        if ($user) {
            $user->is_activated = 1;
            $result = $this->manager->update($user);
        }
    }
    public function updatePhoto()
    {
        $data = json_decode(file_get_contents("php://input"));
        $user = $this->repository->getById($data->id);

        if (!$user) {
            return json_encode(['code' => 0, 'message' => 'No User']);
        }

        $user->photo =  $this->base64ToJPEG($data->photo, 'uploads/profile/photo_' . $user->id . '_' . time() . '.jpg');
        $user->full_path = BASE_URL . $user->photo;
        $result = $this->manager->update($user);

        return json_encode(['code' => 1, 'data' => $result]);
    }

    private function base64ToJPEG($base64_string, $output_file = 'uploads/profile')
    {

        // open the output file for writing
        $ifp = fopen($output_file, 'wb');

        $data = explode(',', $base64_string);
        // we could add validation here with ensuring count( $data ) > 1

        fwrite($ifp, base64_decode($data[0]));

        // clean up the file resource
        fclose($ifp);

        return $output_file;
    }
}
