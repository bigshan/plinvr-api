<?php
namespace Surgiscript\Adapter;

/**
 * @package Surgiscript
 */
use PDODb;

class DatabaseAdapter
{
    public $db = null;
    public function __construct()
    {
        $this->db = new PDODb(['type' => DB_TYPE,
            'host' => DB_HOST,
            'username' => DB_USER,
            'password' => DB_PASSWORD,
            'dbname' => DB_NAME,
            'port' => DB_PORT,
            'prefix' => DB_PREFIX,
            'charset' => DB_CHAR_SET]);
    }

}
